//
//  RestoBarShopViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "RestoBarShopViewController.h"
#import "MainMenuModel.h"
#import "CommonMenuViewController.h"
#import "Constants.h"
#import "RbsModel.h"
#import "Constants.h"

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern NSString *titleName;
extern RbsModel *g_rbs;

@interface RestoBarShopViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *rsbArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
}

@end

@implementation RestoBarShopViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, lblLogo;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
     tvMainService.estimatedRowHeight = 50;
    rsbArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
}

-(void) initView{
    
    //set logo image
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    
    lblLogo.text = NSLocalizedString(@"Resto Bar Shop", nil);
    lblWelcome.text = titleName;
    
    for (int i=0;i< [g_rbs.rsb intValue]; i++){
        
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        NSString *rbsString = [NSString stringWithFormat:@"rsb_%d_rbs_type", i];
        //mainMenu.serviceName = [g_rbs valueForKey:rbsString];
        mainMenu.serviceName = NSLocalizedString([g_rbs valueForKey:rbsString], nil);
        NSLog(@"keywords: %@, %@",rbsString, [g_rbs valueForKey:rbsString]);
        
        mainMenu.serviceIcon = @"ic_folder";
        
        [rsbArray addObject:mainMenu];
        
    }

    
    
    
}
#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [rsbArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RestoBarShopCell * cell = [tableView dequeueReusableCellWithIdentifier:@"RestoBarShopCell"];
    
    if (cell == nil){
        cell = [[RestoBarShopCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RestoBarShopCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) rsbArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
   
    return cell;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) rsbArray[indexPath.row];
    if(mainMenu.serviceName.length == 0){
        return 0;
    }
    return UITableViewAutomaticDimension;
}
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    
    CommonMenuViewController *vc;
    vc = (CommonMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonMenuViewController"];
    
    vc.commonMenuArray = [[NSMutableArray alloc] init];
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    
    //Restaurant 0
    if(row == 0){
        mainMenu.serviceName = g_rbs.rsb_0_rbs_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_rbs.rsb_0_rbs_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_rbs.rsb_0_rbs_map.map_address;
        mainMenu.serviceLat = g_rbs.rsb_0_rbs_map.map_address_lat;
        mainMenu.serviceLng = g_rbs.rsb_0_rbs_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_rbs.rsb_0_rbs_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_rbs.rsb_0_rbs_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Supermarket
    else if(row == 1){
        
        mainMenu.serviceName = g_rbs.rsb_1_rbs_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_rbs.rsb_1_rbs_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_rbs.rsb_1_rbs_map.map_address;
        mainMenu.serviceLat = g_rbs.rsb_1_rbs_map.map_address_lat;
        mainMenu.serviceLng = g_rbs.rsb_1_rbs_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_rbs.rsb_1_rbs_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_rbs.rsb_1_rbs_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Gas Station
    else if (row == 2){
        
        mainMenu.serviceName = g_rbs.rsb_2_rbs_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_rbs.rsb_2_rbs_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_rbs.rsb_2_rbs_map.map_address;
        mainMenu.serviceLat = g_rbs.rsb_2_rbs_map.map_address_lat;
        mainMenu.serviceLng = g_rbs.rsb_2_rbs_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_rbs.rsb_2_rbs_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_rbs.rsb_2_rbs_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Other
    else if(row == 3){
        mainMenu.serviceName = g_rbs.rsb_3_rbs_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_rbs.rsb_3_rbs_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_rbs.rsb_3_rbs_map.map_address;
        mainMenu.serviceLat = g_rbs.rsb_3_rbs_map.map_address_lat;
        mainMenu.serviceLng = g_rbs.rsb_3_rbs_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_rbs.rsb_3_rbs_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_rbs.rsb_3_rbs_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
        
    }
    //Restaurant1
    else if(row == 4){
        
        mainMenu.serviceName = g_rbs.rsb_4_rbs_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_rbs.rsb_4_rbs_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_rbs.rsb_4_rbs_map.map_address;
        mainMenu.serviceLat = g_rbs.rsb_4_rbs_map.map_address_lat;
        mainMenu.serviceLng = g_rbs.rsb_4_rbs_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_rbs.rsb_4_rbs_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_rbs.rsb_4_rbs_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Restaurant2
    else if(row == 5){
        
        mainMenu.serviceName = g_rbs.rsb_5_rbs_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_rbs.rsb_5_rbs_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_rbs.rsb_5_rbs_map.map_address;
        mainMenu.serviceLat = g_rbs.rsb_5_rbs_map.map_address_lat;
        mainMenu.serviceLng = g_rbs.rsb_5_rbs_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_rbs.rsb_5_rbs_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_rbs.rsb_5_rbs_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];

    }
    //Bakery
    else if(row == 6){
        
        mainMenu.serviceName = g_rbs.rsb_6_rbs_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_rbs.rsb_6_rbs_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_rbs.rsb_6_rbs_map.map_address;
        mainMenu.serviceLat = g_rbs.rsb_6_rbs_map.map_address_lat;
        mainMenu.serviceLng = g_rbs.rsb_6_rbs_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_rbs.rsb_6_rbs_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_rbs.rsb_6_rbs_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];

    }
    //Other
    else if(row ==7){
        
        mainMenu.serviceName = g_rbs.rsb_7_rbs_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_rbs.rsb_7_rbs_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_rbs.rsb_7_rbs_map.map_address;
        mainMenu.serviceLat = g_rbs.rsb_7_rbs_map.map_address_lat;
        mainMenu.serviceLng = g_rbs.rsb_7_rbs_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_rbs.rsb_7_rbs_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_rbs.rsb_7_rbs_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Restaurant 3
    else if(row == 8){
        
        mainMenu.serviceName = g_rbs.rsb_8_rbs_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_rbs.rsb_8_rbs_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_rbs.rsb_8_rbs_map.map_address;
        mainMenu.serviceLat = g_rbs.rsb_8_rbs_map.map_address_lat;
        mainMenu.serviceLng = g_rbs.rsb_8_rbs_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_rbs.rsb_8_rbs_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_rbs.rsb_8_rbs_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];

    }
    vc.logoText = @"Resto Bar Shop";
    vc.imvTopBarName = @"ic_top_bar_image_resto_bar_shop.png";
    vc.lblWelcomeName = titleName;
    [self.navigationController pushViewController:vc animated:YES];

}

- (IBAction)onTappedBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
