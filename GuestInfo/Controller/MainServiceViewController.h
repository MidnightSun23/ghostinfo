//
//  MainServiceViewController.h
//  GuestInfo
//
//  Created by MidnightSun on 12/11/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainServiceCell.h"
@import ScrollingFollowView;

@interface MainServiceViewController : UIViewController

@property (strong) NSDictionary *dataDict;

@property (weak, nonatomic) IBOutlet ScrollingFollowView *followView1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topFollowViewConstrain;
@property (weak, nonatomic) IBOutlet UITableView *tvMainService;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;
@property (weak, nonatomic) IBOutlet UILabel *lblLogo;

- (IBAction)onTappedBackBtn:(id)sender;

@end
