//
//  HelpViewController.h
//  GuestInfo
//
//  Created by MidnightSun on 12/21/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController

- (IBAction)onTappedBackBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtView;
@property BOOL isSel_miscel;

@end
