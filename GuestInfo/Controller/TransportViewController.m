//
//  TransportViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "TransportViewController.h"
#import "MainMenuModel.h"
#import "CommonMenuViewController.h"
#import "CommonLabelMenuViewController.h"
#import "Constants.h"

#import "TrAirportModel.h"
#import "TrMotoModel.h"
#import "TrBikeModel.h"
#import "TrCarModel.h"
#import "TrPublicModel.h"
#import "TrTaxiModel.h"
extern TrAirportModel *g_trAirport;
extern TrMotoModel *g_trMoto;
extern TrBikeModel *g_trBike;
extern TrCarModel *g_trCar;
extern TrPublicModel *g_trPublic;
extern TrTaxiModel *g_trTaxi;

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern NSString *titleName;




@interface TransportViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *transportArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;

}

@end

@implementation TransportViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, lblLogo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
        tvMainService.estimatedRowHeight = 50;
    transportArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
}

-(void) initView{
    
    //set logo image
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    
    lblLogo.text = NSLocalizedString(@"Transport", nil);
    lblWelcome.text = titleName;
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Public";
    mainMenu.serviceName = NSLocalizedString(@"Public", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [transportArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Taxi";
    mainMenu.serviceName = NSLocalizedString(@"Taxi", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [transportArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Car Hire";
    mainMenu.serviceName = NSLocalizedString(@"Car Hire", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [transportArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Bike Hire";
    mainMenu.serviceName = NSLocalizedString(@"Bike Hire", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [transportArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Moto Quad Scoot Hire";
    mainMenu.serviceName = NSLocalizedString(@"Moto Quad Scoot Hire", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [transportArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
   // mainMenu.serviceName = @"Airport";
    mainMenu.serviceName = NSLocalizedString(@"Airport", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [transportArray addObject:mainMenu];
    
}
#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [transportArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TransportCell * cell = [tableView dequeueReusableCellWithIdentifier:@"TransportCell"];
    
    if (cell == nil){
        cell = [[TransportCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TransportCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) transportArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    
    CommonMenuViewController *vc;
    vc = (CommonMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonMenuViewController"];
    
    vc.commonMenuArray = [[NSMutableArray alloc] init];
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    
    //Public
    
    if(row == 0){
     
        CommonLabelMenuViewController *vc1;
        vc1 = (CommonLabelMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonLabelMenuViewController"];
        
        vc1.commonMenuArray = [[NSMutableArray alloc] init];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceComment = @"Bus";
        mainMenu.serviceName = g_trPublic.trp_bus;
        mainMenu.serviceIcon = @"ic_bus";
        [vc1.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceComment = @"Metro";
        mainMenu.serviceName = g_trPublic.trp_metro;
        mainMenu.serviceIcon = @"ic_metro";
        [vc1.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceComment = @"Train";
        mainMenu.serviceName = g_trPublic.trp_train;
        mainMenu.serviceIcon = @"ic_train";
        [vc1.commonMenuArray addObject:mainMenu];
        
        vc1.logoText = @"Transport";
        vc1.imvTopBarName = @"ic_top_bar_image_transport.png";
        vc1.lblWelcomeName = titleName;
        [self.navigationController pushViewController:vc1 animated:YES];
        
        return;
        
    }
    //Taxi Hire
    else if (row == 1){
        
        mainMenu.serviceName=g_trTaxi.trt_name;
         [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_trTaxi.trt_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_trTaxi.trt_address.map_address;
        mainMenu.serviceLat = g_trTaxi.trt_address.map_address_lat;
        mainMenu.serviceLng = g_trTaxi.trt_address.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_trTaxi.trt_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = ET;
        mainMenu.serviceName = g_trTaxi.trt_email;
        mainMenu.serviceIcon = @"ic_email";
        [vc.commonMenuArray addObject:mainMenu];
        
    }
    //Car hire
    else if (row == 2){
        
        mainMenu.serviceName = g_trCar.trc_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_trCar.trc_address.map_address;
        mainMenu.serviceLat = g_trCar.trc_address.map_address_lat;
        mainMenu.serviceLng = g_trCar.trc_address.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_trCar.trc_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = ET;
        mainMenu.serviceName = g_trCar.trc_email;
        mainMenu.serviceIcon = @"ic_email";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = FUT;
        mainMenu.serviceName = g_trCar.trc_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
        
    }
    //Bike Hire
    else if (row == 3){
        
        mainMenu.serviceName = g_trBike.trb_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_trBike.trb_address.map_address;
        mainMenu.serviceLat = g_trBike.trb_address.map_address_lat;
        mainMenu.serviceLng = g_trBike.trb_address.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_trBike.trb_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = ET;
        mainMenu.serviceName = g_trBike.trb_email;
        mainMenu.serviceIcon = @"ic_email";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = FUT;
        mainMenu.serviceName = g_trBike.trb_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
        
    }
    //Moto Quard Scoot Hire
    
    else if (row == 4){
        
        mainMenu.serviceName = g_trMoto.trm_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_trMoto.trm_address.map_address;
        mainMenu.serviceLat = g_trMoto.trm_address.map_address_lat;
        mainMenu.serviceLng = g_trMoto.trm_address.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_trMoto.trm_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = ET;
        mainMenu.serviceName = g_trMoto.trm_email;
        mainMenu.serviceIcon = @"ic_email";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = FUT;
        mainMenu.serviceName = g_trMoto.trm_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Airport
    else if (row == 5){
        
        mainMenu.serviceName = g_trAirport.trair_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_trAirport.trair_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_trAirport.trair_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_trAirport.trair_address.map_address;
        mainMenu.serviceLat = g_trAirport.trair_address.map_address_lat;
        mainMenu.serviceLng = g_trAirport.trair_address.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];


    }
    
    vc.logoText = @"Transport";
    vc.imvTopBarName = @"ic_top_bar_image_transport.png";
    vc.lblWelcomeName = titleName;
    [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)onTappedBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
