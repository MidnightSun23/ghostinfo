//
//  SettingViewController.h
//  GuestInfo
//
//  Created by MidnightSun on 12/18/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <UIKit/UIKit.h>
//@property (strong) NSString *postAIN;



@interface SettingViewController : UIViewController<UITextFieldDelegate>
    

@property (weak, nonatomic) IBOutlet UITextField *txtAIN;
@property (weak, nonatomic) UITextField *activeField;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UIButton *btnChangeAccount;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property int index1;

- (IBAction)onTappedOKBtn:(id)sender;
- (IBAction)onTappedRefreshBtn:(id)sender;
- (IBAction)onTappedChangeAccommodationBtn:(id)sender;
- (IBAction)onTappedBackBtn:(id)sender;

@end
