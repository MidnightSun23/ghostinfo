//
//  HelpViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/21/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "HelpViewController.h"

extern NSString *g_miscel;

@interface HelpViewController ()

@end

@implementation HelpViewController

@synthesize txtView, isSel_miscel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
    if (isSel_miscel) {
        NSString *htmlString = [NSString stringWithFormat: @"<font size = 5> %@%@", NSLocalizedString(g_miscel, nil),@"</font>"];
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        txtView.attributedText = attributedString;
        txtView.editable = NO;
        txtView.dataDetectorTypes = UIDataDetectorTypeLink;
    }else{
        
    
    NSString *htmlString = [NSString stringWithFormat: @"<font size = 5> %@%@", NSLocalizedString(@"As an example, you will already have one property back after installing this app, Casa Nipa, so you can try everything once quiet. You can enter 3 accommodations by using the \"Settings\" icon next to each place a property name. Touching the icon takes you to the 'which institution screen, where you choose Edit Property. You enter at the top of the AIN (accommodation info Number) and press OK. Property details are then loaded and displayed. A more detailed version of this guide can be found in the <a href=\"https://www.guestinfo.org/nl/handleiding\"> manual. </a>", nil),@"</font>"];
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    txtView.attributedText = attributedString;
    txtView.editable = NO;
    txtView.dataDetectorTypes = UIDataDetectorTypeLink;
    
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onTappedBackBtn:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
