//
//  MainViewController.h
//  GuestInfo
//
//  Created by MidnightSun on 12/7/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"
@import ScrollingFollowView;
@import AFNetworking;

@interface MainViewController :CommonViewController


@property (weak, nonatomic) IBOutlet ScrollingFollowView *followView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topFollowViewConstrain;
@property (weak, nonatomic) IBOutlet UITableView *tvFollow;

@property (weak, nonatomic) IBOutlet UIView *topView;





@end

