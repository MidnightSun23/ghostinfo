//
//  ComfortViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//


#import "ComfortViewController.h"
#import "CommonMenuViewController.h"
#import "MainMenuModel.h"
#import "ComfortModel.h"
#import "Constants.h"

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern ComfortModel *g_comfort;
extern NSString *titleName;

@interface ComfortViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *comfortArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;

}

@end

@implementation ComfortViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, lblLogo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    tvMainService.estimatedRowHeight = 50;
    comfortArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
}

-(void) initView{
    
    //set logo image
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    
    lblLogo.text = NSLocalizedString(@"Comfort", nil);
    lblWelcome.text = titleName;
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
   // mainMenu.serviceName = @"Coffee and Tea Facilities";
    mainMenu.serviceName = NSLocalizedString(@"Coffee and Tea Facilities", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [comfortArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Laundry";
    mainMenu.serviceName = NSLocalizedString(@"Laundry", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [comfortArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
  //  mainMenu.serviceName = @"Hair";
    mainMenu.serviceName = NSLocalizedString(@"Hair", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [comfortArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
   // mainMenu.serviceName = @"Room Service";
    mainMenu.serviceName = NSLocalizedString(@"Room Service", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [comfortArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
   // mainMenu.serviceName = @"Lugage Service";
    mainMenu.serviceName = NSLocalizedString(@"Lugage Service", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [comfortArray addObject:mainMenu];
    
    
    
}
#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [comfortArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ComfortCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ComfortCell"];
    
    if (cell == nil){
        cell = [[ComfortCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ComfortCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) comfortArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
    /*
     cell.btnCell.tag=indexPath.row;
     [cell.btnCell addTarget:self action:@selector(gotoNextPage:) forControlEvents:UIControlEventTouchUpInside];
     */
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) comfortArray[indexPath.row];
    if(mainMenu.serviceName.length == 0){
        return 0;
    }
    return UITableViewAutomaticDimension;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    CommonMenuViewController *vc;
    vc = (CommonMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonMenuViewController"];
    
    vc.commonMenuArray = [[NSMutableArray alloc] init];
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    
    if (row == 0) {
        mainMenu.serviceName=g_comfort.com_ct_details;
        mainMenu.serviceIcon = @"ic_address";
        [vc.commonMenuArray addObject:mainMenu];
    }
     else if (row==1) {
        mainMenu.serviceName=g_comfort.com_laun_details;
        mainMenu.serviceIcon = @"ic_address";
        [vc.commonMenuArray addObject:mainMenu];
    }
     else if (row==2) {
        mainMenu.serviceName=g_comfort.com_hair_details;
        mainMenu.serviceIcon = @"ic_address";
        [vc.commonMenuArray addObject:mainMenu];
    }

     else if (row==3) {
        mainMenu.serviceName=g_comfort.com_room_details;
        mainMenu.serviceIcon = @"ic_address";
        [vc.commonMenuArray addObject:mainMenu];
    }

     else if (row==4) {
        mainMenu.serviceName=g_comfort.com_lug_details;
        mainMenu.serviceIcon = @"ic_address";
        [vc.commonMenuArray addObject:mainMenu];
    }
    vc.logoText = @"Comfort";
    vc.imvTopBarName = @"ic_top_bar_image_comfort.png";
    vc.lblWelcomeName = titleName;
    [self.navigationController pushViewController:vc animated:YES];

}
- (IBAction)onTappedBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
