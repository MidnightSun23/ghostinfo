//
//  SettingViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/18/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "SettingViewController.h"
#import "MainViewController.h"
#import "MainViewController.h"

NSString *postAIN;
//extern NSString * logoImageUrl;
//extern NSString * categoryImageUrl;
extern BOOL settingFlag;

NSString *settingLogoImageUrl;
NSString *settingCategoryImageUrl;

int idx;
BOOL refreshFlag;

@interface SettingViewController ()
{
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
    
}
@end

@implementation SettingViewController

@synthesize btnOK, btnRefresh,btnChangeAccount,btnBack, txtAIN;
@synthesize activeField;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //set logo image
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (_index1 == 0) {
        settingLogoImageUrl = [userDefaults valueForKey:@"settingLogoImageUrl0"];
        settingCategoryImageUrl = [userDefaults valueForKey:@"settingCategoryImageUrl0"];
    }else if (_index1 == 1){
        settingLogoImageUrl = [userDefaults valueForKey:@"settingLogoImageUrl1"];
        settingCategoryImageUrl = [userDefaults valueForKey:@"settingCategoryImageUrl1"];
    }else{
        settingLogoImageUrl = [userDefaults valueForKey:@"settingLogoImageUrl2"];
        settingCategoryImageUrl = [userDefaults valueForKey:@"settingCategoryImageUrl2"];
    }

        [imvLogo setImageWithURL:[NSURL URLWithString:settingLogoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
        NSLog(@"LogoImageUrl===%@",settingLogoImageUrl);
        
        [imvCategory setImageWithURL:[NSURL URLWithString:settingCategoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
        NSLog(@"CategoryImageUrl===%@", settingCategoryImageUrl);
    
        btnOK.layer.cornerRadius = 10;
        btnRefresh.layer.cornerRadius = 10;
        btnChangeAccount.layer.cornerRadius = 10;
        btnBack.layer.cornerRadius = 10;
    
        [btnRefresh setTitle:NSLocalizedString(@"Refresh", nil) forState:UIControlStateNormal];
        [btnChangeAccount setTitle:NSLocalizedString(@"Change Accommodation", nil) forState:UIControlStateNormal];
        [btnBack setTitle:NSLocalizedString(@"Back", nil) forState:UIControlStateNormal];
        
        btnOK.hidden = YES;
        postAIN = @"";
        txtAIN.text = @"";
        refreshFlag = NO;
    
        NSLog(@"setting0===%@", [userDefaults valueForKey:@"setting0"]);
    
        NSString *post = [userDefaults valueForKey:[NSString stringWithFormat: @"setting%d", _index1]];
        idx = _index1;
        
        if(post != nil){
            
            txtAIN.text = post;
            btnRefresh.hidden = NO;
        }else{
            
            btnRefresh.hidden = YES;
        }
        
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark - TextField Delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    

    [textField resignFirstResponder];
    
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    btnOK.hidden = NO;
    [self.view endEditing:YES];
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
        btnOK.hidden = NO;
        self.activeField = textField;
        self.activeField.delegate = self;
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.activeField = nil;
    btnOK.hidden = NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSRange lowercaseCharRange = [string rangeOfCharacterFromSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    if (lowercaseCharRange.location != NSNotFound) {
        textField.text = [textField.text stringByReplacingCharactersInRange:range
                                                                 withString:[string uppercaseString]];
        return NO;
    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onTappedOKBtn:(id)sender {
    
        postAIN = txtAIN.text;
        settingFlag = YES;
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        
        [userDefaults setValue:txtAIN.text forKey:[NSString stringWithFormat:@"setting%d", _index1]];
        
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onTappedRefreshBtn:(id)sender {
    
     refreshFlag = YES;
     postAIN = txtAIN.text;
     settingFlag = YES;
    
     [self.navigationController popViewControllerAnimated:YES];
    
    
}

- (IBAction)onTappedChangeAccommodationBtn:(id)sender {
    
    
    btnOK.hidden = NO;
}

- (IBAction)onTappedBackBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
