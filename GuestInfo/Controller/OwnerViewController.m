//
//  OwnerViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "OwnerViewController.h"
#import "MainMenuModel.h"
#import "OwnerModel.h"
#import "WebViewController.h"
#import "Constants.h"
#import <MessageUI/MessageUI.h>

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern NSString *titleName;
extern OwnerModel *g_owner;

@interface OwnerViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate, MFMailComposeViewControllerDelegate>{
    
    NSMutableArray *ownerArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
}

@end

@implementation OwnerViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, lblLogo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    tvMainService.estimatedRowHeight = 50;
    ownerArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
}

-(void) initView{
    
    //set logo image
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    
    lblLogo.text = NSLocalizedString(@"Owner", nil);
    lblWelcome.text = titleName;
   
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_firstname;
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_lastname;
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_address;
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_zip;
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_town;
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_country;
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    if(g_owner.o_language.languageArray.count == 0){
        mainMenu.serviceName = @"";
    }else{
    mainMenu.serviceName= g_owner.o_language.languageArray[0];
    for (int i=1; i<g_owner.o_language.languageArray.count;i++){
        
        mainMenu.serviceName = [NSString stringWithFormat:@"%@, %@",mainMenu.serviceName,g_owner.o_language.languageArray[i]];
    }
    }
    [ownerArray addObject:mainMenu];
    
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_telephone;
    mainMenu.serviceIcon = @"ic_call";
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_mobile;
    mainMenu.serviceIcon = @"ic_call";
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_email;
    mainMenu.serviceIcon = @"ic_email";
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_facebook;
    mainMenu.serviceIcon = @"ic_facebookpage";
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_twitter;
    mainMenu.serviceIcon = @"ic_twitter";
    [ownerArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_owner.o_skype;
    mainMenu.serviceIcon = @"ic_skype";
    [ownerArray addObject:mainMenu];
    
    
}

#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}
#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [ownerArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OwnerCell * cell = [tableView dequeueReusableCellWithIdentifier:@"OwnerCell"];
    
    if (cell == nil){
        cell = [[OwnerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OwnerCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) ownerArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
    
    /*
     cell.btnCell.tag=indexPath.row;
     [cell.btnCell addTarget:self action:@selector(gotoNextPage:) forControlEvents:UIControlEventTouchUpInside];
     */
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) ownerArray[indexPath.row];
    if(mainMenu.serviceName.length == 0){
        return 0;
    }
    
    return UITableViewAutomaticDimension;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row= indexPath.row;
    if(row == 7){
    //    o_telephone
    NSString *phoneNumberString = g_owner.o_telephone;
    phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumberString = [NSString stringWithFormat:@"tel:%@", phoneNumberString];
    NSURL *phoneNumberURL = [NSURL URLWithString:phoneNumberString];
    [[UIApplication sharedApplication] openURL:phoneNumberURL];
    
    NSLog(@"PhoneNumber=======%@", phoneNumberString);
    
}else if (row == 8){
    
    //    o_mobile
    NSString *phoneNumberString = g_owner.o_mobile;
    phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumberString = [NSString stringWithFormat:@"tel:%@", phoneNumberString];
    NSURL *phoneNumberURL = [NSURL URLWithString:phoneNumberString];
    [[UIApplication sharedApplication] openURL:phoneNumberURL];
    
    NSLog(@"PhoneNumber=======%@", phoneNumberString);
    
}else if (row == 9){
    
    if ([MFMailComposeViewController canSendMail])
    {
        // Email Subject
        NSString *emailTitle = @"";
        // Email Content
        NSString *messageBody = @"";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:g_owner.o_email];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    else
    {
        NSLog(@"This device cannot send email");
    }

    
}else if (row == 10){
    
    //o_facebook
      if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:g_owner.o_facebook]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:g_owner.o_facebook]];
        
    }else{
        
        WebViewController *vcWeb;
        vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
        vcWeb.webURL = g_owner.o_facebook;
        [self.navigationController pushViewController:vcWeb animated:YES];
    }
    
    
}else if (row == 11){
    //o_twitter

    NSString * twitterURL = [NSString stringWithFormat:@"http://twitter.com/%@",[g_owner.o_twitter stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:twitterURL]]) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterURL]];
        
    }else{
        
        WebViewController *vcWeb;
        vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
        
        vcWeb.webURL = [NSString stringWithFormat:@"http://twitter.com/%@",g_owner.o_twitter];
        NSLog(@"twitterlink=======%@", vcWeb.webURL );
        [self.navigationController pushViewController:vcWeb animated:YES];
        
    }
    
}else if(row == 12) {
   
    //o_skype
    
    NSString *userNameString = g_owner.o_skype;
    
    BOOL installed = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"skype:"]];
    
    if(installed)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"skype:%@?call", userNameString]]];
    }
    else
    {
     
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/skype-for-iphone/id304878510?mt=8"]];
        
    }
    
}


}

- (IBAction)onTappedBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
