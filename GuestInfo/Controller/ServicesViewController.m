//
//  ServicesViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "ServicesViewController.h"
#import "MainMenuModel.h"
#import "Constants.h"
#import "CommonMenuViewController.h"
#import "CommonLabelMenuViewController.h"
#import "ServicesModel.h"

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern NSString *titleName;
extern ServicesModel *g_services;
extern NSString *titleName;


@interface ServicesViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *servicesArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
}

@end

@implementation ServicesViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome,lblLogo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    
    servicesArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
}

-(void) initView{
    
    //set logo image
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    
    lblLogo.text = NSLocalizedString(@"Services", nil);
    lblWelcome.text = titleName;
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
   // mainMenu.serviceName = @"Check IN and OUT";
    mainMenu.serviceName = NSLocalizedString(@"Check IN and OUT", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [servicesArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"ATM Money Dispenser";
    mainMenu.serviceName = NSLocalizedString(@"ATM Money Dispenser", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [servicesArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Wifi and Internet";
    mainMenu.serviceName = NSLocalizedString(@"Wifi and Internet", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [servicesArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Parking";
    mainMenu.serviceName = NSLocalizedString(@"Parking", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [servicesArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
   // mainMenu.serviceName = @"Radio and TV";
    mainMenu.serviceName = NSLocalizedString(@"Radio and TV", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [servicesArray addObject:mainMenu];
    
 
    
}
#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [servicesArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ServicesCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ServicesCell"];
    
    if (cell == nil){
        cell = [[ServicesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ServicesCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) servicesArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
   
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    
    CommonMenuViewController *vc;
    vc = (CommonMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonMenuViewController"];
    
    vc.commonMenuArray = [[NSMutableArray alloc] init];
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    
    //Check IN and OUT
    
    if(row == 0){
        
        
        CommonLabelMenuViewController *vc1;
        vc1 = (CommonLabelMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonLabelMenuViewController"];
        
        vc1.commonMenuArray = [[NSMutableArray alloc] init];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceComment = @"Check In";
        mainMenu.serviceName = g_services.ser_checkintime;
        mainMenu.serviceIcon = @"ic_clock";
        [vc1.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceComment = @"Check Out";
        mainMenu.serviceName = g_services.ser_checkouttime;
        mainMenu.serviceIcon = @"ic_clock";
        [vc1.commonMenuArray addObject:mainMenu];
        
        vc1.logoText = @"Services";
        vc1.imvTopBarName = @"ic_top_bar_image_services.png";
        vc1.lblWelcomeName = titleName;
        [self.navigationController pushViewController:vc1 animated:YES];
        
        return;
        
    }
    //ATM Money Dispenser
    
    else if (row == 1){
        
        mainMenu.serviceName=g_services.ser_atm_address;
        [vc.commonMenuArray addObject:mainMenu];
        
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_services.ser_atm_location.map_address;
        mainMenu.serviceLat = g_services.ser_atm_location.map_address_lat;
        mainMenu.serviceLng = g_services.ser_atm_location.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
 

        
        
    }
    //Wifi and Internet
    
    else if (row == 2){
        
        CommonLabelMenuViewController *vc1;
        vc1 = (CommonLabelMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonLabelMenuViewController"];
        
        vc1.commonMenuArray = [[NSMutableArray alloc] init];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceComment = @"Wifi Password";
        mainMenu.serviceName = g_services.ser_wifi_pass;
        mainMenu.serviceIcon = @"ic_password";
        [vc1.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
       // mainMenu.serviceComment = @"Check Out";
        mainMenu.serviceName = g_services.ser_wifi_details;
        mainMenu.serviceIcon = @"ic_address";
        [vc1.commonMenuArray addObject:mainMenu];
        
        vc1.logoText = @"Services";
        vc1.imvTopBarName = @"ic_top_bar_image_services.png";
        vc1.lblWelcomeName = titleName;
        [self.navigationController pushViewController:vc1 animated:YES];
        
        return;

    }
    //Parking
    
    else if (row == 3){
        
        mainMenu.serviceName = g_services.ser_park_details;
        mainMenu.serviceIcon = @"ic_address";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_services.ser_park_map.map_address;
        mainMenu.serviceLat = g_services.ser_park_map.map_address_lat;
        mainMenu.serviceLng = g_services.ser_park_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        

        
        
        
    }
    //Radio and TV
    
    else if (row == 4){
        
        mainMenu.serviceName = g_services.ser_tv_details;
        mainMenu.serviceIcon = @"ic_address";
        [vc.commonMenuArray addObject:mainMenu];
        
    }
    
       vc.logoText = @"Services";
       vc.imvTopBarName = @"ic_top_bar_image_transport.png";
       vc.lblWelcomeName = titleName;
       [self.navigationController pushViewController:vc animated:YES];
}


- (IBAction)onTappedBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
