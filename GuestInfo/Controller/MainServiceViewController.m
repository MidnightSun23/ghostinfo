//
//  MainServiceViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/11/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//



#import "MainServiceViewController.h"
#import "MainMenuModel.h"
#import "EmergencyViewController.h"
#import "Constants.h"
#import "MainViewController.h"
#import "AccommodationModel.h"
#import "CommonUtils.h"
#import "HelpViewController.h"

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern AccommodationModel *g_accommodation;
extern int buttonTag;


@interface MainServiceViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *mainServiceArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
     NSUserDefaults *userDefaults;
}

@end

@implementation MainServiceViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome,lblLogo;

- (void)viewDidLoad {
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
  
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
   // CGFloat statusBarHeight = [UIApplication sharedApplication]. statusBarFrame.size.height;
   // CGFloat topViewHeight = 70.0;
    
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;

    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    
    mainServiceArray = [[NSMutableArray alloc] init];
    
    //settingLogoImage and settingCategoryImage Url
    
    if (buttonTag == 0) {

        [userDefaults setValue:logoImageUrl forKey:@"settingLogoImageUrl0"];
        [userDefaults setValue:categoryImageUrl forKey:@"settingCategoryImageUrl0"];
        
    }else if (buttonTag == 1){

        [userDefaults setValue:logoImageUrl forKey:@"settingLogoImageUrl1"];
        [userDefaults setValue:categoryImageUrl forKey:@"settingCategoryImageUrl1"];
    }else{
 
        [userDefaults setValue:logoImageUrl forKey:@"settingLogoImageUrl2"];
        [userDefaults setValue:categoryImageUrl forKey:@"settingCategoryImageUrl2"];
    }
    
    [self initView];
    
    
  /*
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight+statusBarHeight-topViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    */
  
    
    
}

-(void) initView{
    
    //set logo image
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    NSLog(@"LogoImageUrl===%@",logoImageUrl);
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    NSLog(@"CategoryImageUrl===%@", categoryImageUrl);
    
   
    
    NSString *htmlString = [CommonUtils convertHTML:g_accommodation.a_welcome];
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    lblWelcome.attributedText = attributedString;
    
    lblLogo.text = NSLocalizedString(@"Accommodation", nil);
    
    MainMenuModel *mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Emergency", nil);
    mainServer.serviceIcon = @"ic_emmergency";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Miscellaneous", nil);
   // mainServer.serviceIcon = @"ic_accomodation";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Accommodation", nil);
    mainServer.serviceIcon = @"ic_accomodation";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Local Contact Person", nil);
    mainServer.serviceIcon = @"ic_local_contact_person";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Owner", nil);
    mainServer.serviceIcon = @"ic_owner";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Food & Drink", nil);
    mainServer.serviceIcon = @"ic_food_drink";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Transport", nil);
    mainServer.serviceIcon = @"ic_transport";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Services", nil);
    mainServer.serviceIcon = @"ic_services";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Attractions", nil);
    mainServer.serviceIcon = @"ic_attrection";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Entertainment", nil);
    mainServer.serviceIcon = @"ic_entertainment";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Resto Bar Shop", nil);
    mainServer.serviceIcon = @"ic_restobar_shop";
    [mainServiceArray addObject:mainServer];
    
    mainServer = [[MainMenuModel alloc] init];
    mainServer.serviceName = NSLocalizedString(@"Comfort", nil);
    mainServer.serviceIcon = @"ic_comfort";
    [mainServiceArray addObject:mainServer];
}

#pragma mark - ScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 11;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainServiceCell * cell = [tableView dequeueReusableCellWithIdentifier:@"MainServiceCell"];
    
    if (cell == nil){
        cell = [[MainServiceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainServiceCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    

    MainMenuModel *mainServer = [[MainMenuModel alloc] init];
    mainServer = (MainMenuModel *)mainServiceArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainServer.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainServer.serviceIcon];
    

    
    cell.btnCell.tag=indexPath.row;
    [cell.btnCell addTarget:self action:@selector(gotoNextPage:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)gotoNextPage:(UIButton*) button{
    
        HelpViewController *vcHelp;
        
        UIViewController *vc;
        switch (button.tag) {
            case 0:
                
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"EmergencyViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
            case 1:
             
                vcHelp = [self.storyboard instantiateViewControllerWithIdentifier: @"HelpViewController"];
                vcHelp.isSel_miscel = YES;
                [self.navigationController pushViewController:vcHelp animated:YES];
                
                break;
                
            case 2:
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"AccomodationViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
                
            case 3:
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"LocalContactPersonViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
                
            case 4:
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"OwnerViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
                
            case 5:
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"FoodandDrinkViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
                
            case 6:
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"TransportViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
                
            case 7:
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"ServicesViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
                
            case 8:
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"AttractionsViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
                
            case 9:
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"EntertainmentViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
                
            case 10:
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"RestoBarShopViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
                
            case 11:
                vc = [self.storyboard instantiateViewControllerWithIdentifier: @"ComfortViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
                break;
                
            default:
                break;
        }
}

- (IBAction)onTappedBackBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
