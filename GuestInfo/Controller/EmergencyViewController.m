//
//  EmergencyViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//
#import "MainViewController.h"
#import "EmergencyViewController.h"
#import "MainMenuModel.h"
#import "EmmergencyModel.h"
#import "CommonMenuViewController.h"
#import "Constants.h"
extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern NSString *titleName;

extern EmmergencyModel *g_emmergency;

@interface EmergencyViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *emergencyArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
}

@end

@implementation EmergencyViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, lblLogo;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    tvMainService.estimatedRowHeight = 50;
    emergencyArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
    
}
-(void) initView{
    
    //set logo image
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    NSLog(@"LogoImageUrl===%@",logoImageUrl);
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    NSLog(@"CategoryImageUrl===%@", categoryImageUrl);
    lblWelcome.text = titleName;
    lblLogo.text = NSLocalizedString(@"Emergency", nil);
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"General";
    mainMenu.serviceName = NSLocalizedString(@"General", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [emergencyArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Fire";
    mainMenu.serviceName = NSLocalizedString(@"Fire", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [emergencyArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Ambulance";
    mainMenu.serviceName = NSLocalizedString(@"Ambulance", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [emergencyArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Police";
    mainMenu.serviceName = NSLocalizedString(@"Police", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [emergencyArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
   // mainMenu.serviceName = @"Road Assistance";
    mainMenu.serviceName = NSLocalizedString(@"Road Assistance", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [emergencyArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Doctor";
    mainMenu.serviceName = NSLocalizedString(@"Doctor", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [emergencyArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
   // mainMenu.serviceName = @"Pharmacy";
    mainMenu.serviceName = NSLocalizedString(@"Pharmacy", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [emergencyArray addObject:mainMenu];
    
}

#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}
#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [emergencyArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EmergencyCell * cell = [tableView dequeueReusableCellWithIdentifier:@"EmergencyCell"];
    
    if (cell == nil){
        cell = [[EmergencyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EmergencyCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) emergencyArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
    
 
    return cell;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    
    CommonMenuViewController *vc;
    vc = (CommonMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonMenuViewController"];
    
    vc.commonMenuArray = [[NSMutableArray alloc] init];
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    
    if(row == 0){
        
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_emmergency.em_general;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
    }else if (row == 1){
        
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_emmergency.em_fire;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
    }else if (row == 2){
        
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_emmergency.em_ambulance;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
    }else if (row == 3){
        
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_emmergency.em_police;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
    }else if (row == 4){
        
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_emmergency.em_assistance;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
    }else if (row == 5){
        
        
        mainMenu.serviceName = g_emmergency.em_docter_name;
        [vc.commonMenuArray addObject:mainMenu];
       
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
         mainMenu.serviceId = AT;
        mainMenu.serviceName = g_emmergency.em_docter_address.map_address;
        mainMenu.serviceLat = g_emmergency.em_docter_address.map_address_lat;
        mainMenu.serviceLng = g_emmergency.em_docter_address.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_emmergency.em_docter_telephone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
    }else {
        
        
        mainMenu.serviceName = g_emmergency.em_pharmacy_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_emmergency.em_pharmacy_address.map_address;
        mainMenu.serviceLat = g_emmergency.em_pharmacy_address.map_address_lat;
        mainMenu.serviceLng = g_emmergency.em_pharmacy_address.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_emmergency.em_pharmacy_telephone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
    }
    
    vc.logoText = @"Emergency";
    vc.imvTopBarName = @"ic_top_bar_image_emergency.png";
    vc.lblWelcomeName = titleName;
    NSLog(@"titleName===%@", titleName);
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onTappedBackBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
