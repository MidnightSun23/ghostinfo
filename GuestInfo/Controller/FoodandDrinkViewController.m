//
//  FoodandDrinkViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "FoodandDrinkViewController.h"
#import "MainMenuModel.h"
#import "FoodandDrinkModel.h"
#import "CommonMenuViewController.h"
#import "Constants.h"

extern FoodandDrinkModel * g_foodandDrink;

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern NSString *titleName;

@interface FoodandDrinkViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *fdArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
}

@end

@implementation FoodandDrinkViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, lblLogo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    
    fdArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initView{
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    
    lblLogo.text = NSLocalizedString(@"Food & Drink", nil);
    lblWelcome.text = titleName;
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
   // mainMenu.serviceName = @"Breakfast";
    mainMenu.serviceName = NSLocalizedString(@"Breakfast", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [fdArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Lunch";
    mainMenu.serviceName = NSLocalizedString(@"Lunch", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [fdArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    //mainMenu.serviceName = @"Dinner";
    mainMenu.serviceName = NSLocalizedString(@"Dinner", nil);
    mainMenu.serviceIcon = @"ic_folder";
    [fdArray addObject:mainMenu];
 
    
    
}
#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [fdArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FoodandDrinkCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FoodandDrinkCell"];
    
    if (cell == nil){
        cell = [[FoodandDrinkCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FoodandDrinkCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) fdArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
 
     cell.btnCell.tag=indexPath.row;
     [cell.btnCell addTarget:self action:@selector(gotoNextPage:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
-(void)gotoNextPage:(UIButton*) button{
    
    CommonMenuViewController *vc;
    vc = (CommonMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonMenuViewController"];
    
    vc.commonMenuArray = [[NSMutableArray alloc] init];

    
    if(button.tag == 0){
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_foodandDrink.fdb_time;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_foodandDrink.fdb_where;
        [vc.commonMenuArray addObject:mainMenu];
    }else if(button.tag == 1){
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_foodandDrink.fdl_time;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_foodandDrink.fdl_where;
        [vc.commonMenuArray addObject:mainMenu];
    }else{
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_foodandDrink.fdd_time;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_foodandDrink.fdd_where;
        [vc.commonMenuArray addObject:mainMenu];
    }
    
    vc.logoText = @"Food & Drink";
    vc.imvTopBarName = @"ic_top_bar_image_fooddrink.png";
    vc.lblWelcomeName = titleName;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onTappedBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
