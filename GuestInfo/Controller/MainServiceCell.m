//
//  MainServiceCell.m
//  GuestInfo
//
//  Created by MidnightSun on 12/11/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "MainServiceCell.h"

@implementation MainServiceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
