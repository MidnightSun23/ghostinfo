//
//  FoodandDrinkViewController.h
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FoodandDrinkCell.h"
@import ScrollingFollowView;

@interface FoodandDrinkViewController : UIViewController

@property (weak, nonatomic) IBOutlet ScrollingFollowView *followView1;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topFollowViewConstrain;

@property (weak, nonatomic) IBOutlet UITableView *tvMainService;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;
@property (weak, nonatomic) IBOutlet UILabel *lblLogo;

- (IBAction)onTappedBackBtn:(id)sender;

@end
