//
//  AccomodationViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "AccomodationViewController.h"
#import "MainMenuModel.h"
#import "AccommodationModel.h"
#import "CommonMenuViewController.h"
#import "WebViewController.h"
#import "Constants.h"
#import "CommonUtils.h"



extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern AccommodationModel *g_accommodation;

@interface AccomodationViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *accomodationArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
}

@end

@implementation AccomodationViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, welcomeView, lblLogo;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    tvMainService.estimatedRowHeight = 50;
    accomodationArray = [[NSMutableArray alloc] init];
    
 
    [self initView];
}

-(void) initView{
    
    //set logo image
    
    
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];

    
    NSString *htmlString = [CommonUtils convertHTML:g_accommodation.a_welcome];
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
    lblWelcome.attributedText = attributedString;
    
    
    lblLogo.text = NSLocalizedString(@"Accommodation", nil);
   
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_accommodation.a_address;
    [accomodationArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_accommodation.a_zip;
    [accomodationArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_accommodation.a_town;
    [accomodationArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_accommodation.a_country;
    [accomodationArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_accommodation.a_map.map_address;
    mainMenu.serviceIcon = @"ic_map";
    [accomodationArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_accommodation.a_website;
    mainMenu.serviceIcon = @"ic_browser";
    [accomodationArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_accommodation.a_facebook;
    mainMenu.serviceIcon = @"ic_facebookpage";
    [accomodationArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_accommodation.a_twitter;
    mainMenu.serviceIcon = @"ic_twitter";
    [accomodationArray addObject:mainMenu];
    
    mainMenu = [[MainMenuModel alloc] init];
    mainMenu.serviceName = g_accommodation.a_telephone;
    mainMenu.serviceIcon = @"ic_call";
    [accomodationArray addObject:mainMenu];
    
}
#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}
#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [accomodationArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AccomodationCell * cell = [tableView dequeueReusableCellWithIdentifier:@"AccomodationCell"];
    
    if (cell == nil){
        cell = [[AccomodationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AccomodationCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) accomodationArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
    
  
    
    /*
     cell.btnCell.tag=indexPath.row;
     [cell.btnCell addTarget:self action:@selector(gotoNextPage:) forControlEvents:UIControlEventTouchUpInside];
     */
    return cell;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) accomodationArray[indexPath.row];
    if(mainMenu.serviceName.length == 0){
        return 0;
    }
    
    return UITableViewAutomaticDimension;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row= indexPath.row;
       if(row == 4){
           
           NSString *address = [NSString stringWithFormat:@"%@",g_accommodation.a_map.map_address];
           NSString *lat = [NSString stringWithFormat:@"%@",g_accommodation.a_map.map_address_lat];
           NSString *lng = [NSString stringWithFormat:@"%@",g_accommodation.a_map.map_address_lng];
           
           NSLog(@"address===%@", address);
           NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?q=%@&center=%@,%@&zoom=14&views=traffic",
                            [address stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]],[lat stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]],[lng stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
           
           if ([[UIApplication sharedApplication] canOpenURL:
                [NSURL URLWithString:@"comgooglemaps://"]]) {
               [[UIApplication sharedApplication] openURL:
                [NSURL URLWithString:url]];
           } else {
               
                [self showAlertDialog:@"" message:@"Can't use comgooglemaps! Please install the google map App" positive:@"OK" negative:nil sender:self];
             //  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/us/app/google-maps-navigation-transit/id585027354?mt=8"]];
               
               
                NSLog(@"Can't use comgooglemaps://");
               
               
                }
           
    }else if (row == 5){
        
    //a_website
        
        WebViewController *vcWeb;
        vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
        vcWeb.webURL = g_accommodation.a_website;
        [self.navigationController pushViewController:vcWeb animated:YES];
        
    }else if (row == 6){
    //a_facebook
       /*
        WebViewController *vcWeb;
        vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
        vcWeb.webURL = g_accommodation.a_facebook;
        [self.navigationController pushViewController:vcWeb animated:YES];
        */
        
       
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:g_accommodation.a_facebook]]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:g_accommodation.a_facebook]];
            
        }else{
            
            WebViewController *vcWeb;
            vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            vcWeb.webURL = g_accommodation.a_facebook;
            [self.navigationController pushViewController:vcWeb animated:YES];
        }
        
        
    }else if (row == 7){
    //a_twitter
       /*
        WebViewController *vcWeb;
        vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
       
        vcWeb.webURL = [NSString stringWithFormat:@"http://twitter.com/%@",g_accommodation.a_twitter];
        NSLog(@"twitterlink=======%@", vcWeb.webURL );
        [self.navigationController pushViewController:vcWeb animated:YES];
        */
        
        NSString * twitterURL = [NSString stringWithFormat:@"http://twitter.com/%@",[g_accommodation.a_twitter stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
       
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:twitterURL]]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterURL]];
            
        }else{
            
            WebViewController *vcWeb;
            vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            
            vcWeb.webURL = [NSString stringWithFormat:@"http://twitter.com/%@",g_accommodation.a_twitter];
            NSLog(@"twitterlink=======%@", vcWeb.webURL );
            [self.navigationController pushViewController:vcWeb animated:YES];

        }
        
    }else if(row == 8){
    //    a_telephone
        NSString *phoneNumberString = g_accommodation.a_telephone;
        phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
        phoneNumberString = [NSString stringWithFormat:@"tel:%@", phoneNumberString];
        NSURL *phoneNumberURL = [NSURL URLWithString:phoneNumberString];
        [[UIApplication sharedApplication] openURL:phoneNumberURL];
        
        NSLog(@"PhoneNumber=======%@", phoneNumberString);
        
    }
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTappedBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
