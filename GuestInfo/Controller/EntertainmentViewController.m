//
//  EntertainmentViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//


#import "EntertainmentViewController.h"
#import "MainMenuModel.h"
#import "CommonMenuViewController.h"
#import "Constants.h"
#import "EntertainmentModel.h"

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern NSString *titleName;
extern EntertainmentModel *g_entertainment;

@interface EntertainmentViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *entertainmentArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;

}

@end

@implementation EntertainmentViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, lblLogo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    tvMainService.estimatedRowHeight = 50;
    entertainmentArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
}
-(void) initView{
    
    //set logo image
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    
    lblLogo.text = NSLocalizedString(@"Entertainment", nil);
    lblWelcome.text = titleName;
    
    for (int i=0;i< [g_entertainment.ent intValue]; i++){
        
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        NSString *entertainmentString = [NSString stringWithFormat:@"ent_%d_ent_type", i];
        mainMenu.serviceName = [g_entertainment valueForKey:entertainmentString];
       NSLog(@"keywords: %@, %@",entertainmentString, [g_entertainment valueForKey:entertainmentString]);
        
        mainMenu.serviceIcon = @"ic_folder";
        
        [entertainmentArray addObject:mainMenu];

    }
}
#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [g_entertainment.ent intValue];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    EntertainmentCell * cell = [tableView dequeueReusableCellWithIdentifier:@"EntertainmentCell"];
    
    if (cell == nil){
        cell = [[EntertainmentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EntertainmentCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) entertainmentArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
   
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) entertainmentArray[indexPath.row];
    if(mainMenu.serviceName.length == 0){
        return 0;
    }
    return UITableViewAutomaticDimension;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    
    CommonMenuViewController *vc;
    vc = (CommonMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonMenuViewController"];
    
    vc.commonMenuArray = [[NSMutableArray alloc] init];
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    
    //Casino
    if(row == 0){
        mainMenu.serviceName = g_entertainment.ent_0_ent_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_entertainment.ent_0_ent_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_entertainment.ent_0_ent_map.map_address;
        mainMenu.serviceLat = g_entertainment.ent_0_ent_map.map_address_lat;
        mainMenu.serviceLng = g_entertainment.ent_0_ent_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_entertainment.ent_0_ent_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_entertainment.ent_0_ent_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Show
    else if(row == 1){
        mainMenu.serviceName = g_entertainment.ent_1_ent_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_entertainment.ent_1_ent_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_entertainment.ent_1_ent_map.map_address;
        mainMenu.serviceLat = g_entertainment.ent_1_ent_map.map_address_lat;
        mainMenu.serviceLng = g_entertainment.ent_1_ent_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_entertainment.ent_1_ent_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_entertainment.ent_1_ent_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Swimming Pool
   else  if(row == 2){
        mainMenu.serviceName = g_entertainment.ent_2_ent_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_entertainment.ent_2_ent_name;
        [vc.commonMenuArray addObject:mainMenu];
        

        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_entertainment.ent_2_ent_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_entertainment.ent_2_ent_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Gym
    else if(row == 3){
        mainMenu.serviceName = g_entertainment.ent_3_ent_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_entertainment.ent_3_ent_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_entertainment.ent_3_ent_map.map_address;
        mainMenu.serviceLat = g_entertainment.ent_3_ent_map.map_address_lat;
        mainMenu.serviceLng = g_entertainment.ent_3_ent_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_entertainment.ent_3_ent_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_entertainment.ent_3_ent_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Hairdresser
    else if(row == 4){
        mainMenu.serviceName = g_entertainment.ent_4_ent_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_entertainment.ent_4_ent_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_entertainment.ent_4_ent_map.map_address;
        mainMenu.serviceLat = g_entertainment.ent_4_ent_map.map_address_lat;
        mainMenu.serviceLng = g_entertainment.ent_4_ent_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_entertainment.ent_4_ent_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_entertainment.ent_4_ent_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Beauty Center
    else if(row == 5){
        mainMenu.serviceName = g_entertainment.ent_5_ent_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_entertainment.ent_5_ent_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_entertainment.ent_5_ent_map.map_address;
        mainMenu.serviceLat = g_entertainment.ent_5_ent_map.map_address_lat;
        mainMenu.serviceLng = g_entertainment.ent_5_ent_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_entertainment.ent_5_ent_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_entertainment.ent_5_ent_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Club
    else if(row == 6){
        mainMenu.serviceName = g_entertainment.ent_6_ent_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_entertainment.ent_6_ent_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_entertainment.ent_6_ent_map.map_address;
        mainMenu.serviceLat = g_entertainment.ent_6_ent_map.map_address_lat;
        mainMenu.serviceLng = g_entertainment.ent_6_ent_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_entertainment.ent_6_ent_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_entertainment.ent_6_ent_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Disco
    else if(row == 7){
        mainMenu.serviceName = g_entertainment.ent_7_ent_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_entertainment.ent_7_ent_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_entertainment.ent_7_ent_map.map_address;
        mainMenu.serviceLat = g_entertainment.ent_7_ent_map.map_address_lat;
        mainMenu.serviceLng = g_entertainment.ent_7_ent_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_entertainment.ent_7_ent_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_entertainment.ent_7_ent_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    //Movie
    else if(row == 8){
        mainMenu.serviceName = g_entertainment.ent_8_ent_type;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceName = g_entertainment.ent_8_ent_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_entertainment.ent_8_ent_map.map_address;
        mainMenu.serviceLat = g_entertainment.ent_8_ent_map.map_address_lat;
        mainMenu.serviceLng = g_entertainment.ent_8_ent_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = PT;
        mainMenu.serviceName = g_entertainment.ent_8_ent_phone;
        mainMenu.serviceIcon = @"ic_call";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_entertainment.ent_8_ent_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
    }
    
    
    vc.logoText = @"Entertainment";
    vc.imvTopBarName = @"ic_top_bar_image_entertainment.png";
    vc.lblWelcomeName = titleName;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onTappedBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
