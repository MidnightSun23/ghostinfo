//
//  MainViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/7/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//



#import "MainViewController.h"
#import "MainServiceViewController.h"
#import "SettingViewController.h"
#import "HelpViewController.h"
#import "MainCell.h"
#import "EmmergencyModel.h"
#import "AccommodationModel.h"
#import "LocalContactPersonModel.h"
#import "OwnerModel.h"
#import "FoodandDrinkModel.h"
#import "TrTaxiModel.h"
#import "TrCarModel.h"
#import "TrBikeModel.h"
#import "TrMotoModel.h"
#import "TrAirportModel.h"
#import "TrPublicModel.h"
#import "ComfortModel.h"
#import "RbsModel.h"
#import "EntertainmentModel.h"
#import "AttractionModel.h"
#import "ServicesModel.h"
#import "Constants.h"
@import SDWebImage;


extern EmmergencyModel *g_emmergency;
extern AccommodationModel *g_accommodation;
extern LocalContactPersonModel *g_localContactPerson;
extern OwnerModel *g_owner;
extern FoodandDrinkModel * g_foodandDrink;
extern TrTaxiModel *g_trTaxi;
extern TrCarModel *g_trCar;
extern TrBikeModel *g_trBike;
extern TrMotoModel *g_trMoto;
extern TrAirportModel *g_trAirport;
extern TrPublicModel *g_trPublic;
extern ComfortModel *g_comfort;
extern RbsModel *g_rbs;
extern EntertainmentModel *g_entertainment;
extern AttractionModel *g_attraction;
extern ServicesModel *g_services;
NSString *g_miscel;
extern NSString *postAIN;
extern int idx;
extern BOOL refreshFlag;

NSString * logoImageUrl;
NSString * categoryImageUrl;
NSString * post;
NSString * titleName;
int buttonTag;


BOOL settingFlag;
BOOL offlineFlag;

@interface MainViewController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
{

    BOOL serverIsConnected;
    NSString *post;
    __weak IBOutlet UITableView *tblData;
    NSUserDefaults *userDefaults;
    
    __weak IBOutlet UIImageView *imvAttr0;
    __weak IBOutlet UIImageView *imvAttr1;
    __weak IBOutlet UIImageView *imvAttr2;
    __weak IBOutlet UIImageView *imvAttr3;
    __weak IBOutlet UIImageView *imvAttr4;
    
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvPicture;
    
    int imageAllLoaded;
    
    
}

@end

@implementation MainViewController

@synthesize followView, topFollowViewConstrain, tvFollow, topView;

- (void)viewDidLoad {
    

    userDefaults = [NSUserDefaults standardUserDefaults];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
 
    CGFloat scrollingFollowViewHeight = followView.frame.size.height;
    CGFloat statusBarHeight = [UIApplication sharedApplication]. statusBarFrame.size.height;
    CGFloat topViewHeight = 70.0;
    
    tvFollow.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [followView setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight+statusBarHeight-topViewHeight minFollowPoint:0 allowHalfDisplay:NO];
   
    serverIsConnected = NO;
    settingFlag = NO;
    offlineFlag = NO;
    NSString *initPost;
    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    
    NSLog(@"LanguageCode===%@", languageCode);
    if ([languageCode isEqualToString:@"en"]) {
        
        initPost = [NSString stringWithFormat:@"EN228"];
    }else if([languageCode isEqualToString:@"nl"]){
        initPost = [NSString stringWithFormat:@"NL350"];
    }else if([languageCode isEqualToString:@"fr"]){
        initPost = [NSString stringWithFormat:@"FR228"];
    }else{
        initPost = [NSString stringWithFormat:@"EN228"];
    }
    
    
   // NSString *initPost = [NSString stringWithFormat:@"3565"];
    
    
    if ((postAIN == nil)||([postAIN isEqualToString:initPost])) {
        
        post = initPost;
        
    }else{
    
        
        [self showAlertDialog:@"" message:@"Invalid PostID" positive:@"OK" negative:nil sender:self];
        
         }
    //NSLog(@"postAIN=====>>>>%@", postAIN);


}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewWillAppear:(BOOL)animated{
    
        if(settingFlag == YES){
            
            settingFlag = NO;
            offlineFlag = NO;
            //NSLog(@"postAIN=====>>>>%@", postAIN);
            
            post = postAIN;
            
            [tblData reloadData];
            
            if (refreshFlag == YES) {
                if (idx == 0) {
                    buttonTag = 0;
                }else if (idx == 1){
                    buttonTag = 1;
                }else{
                    buttonTag = 2;
                }
                
                [self getDataFromServer];
                
            }else{
                
            [self getData];
            
            }
            
        }else{
    
            settingFlag = FALSE;
            logoImageUrl = @"";
            categoryImageUrl = @"";
        }
}

-(BOOL) getConnectionCount {
    

    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    int day = (int)[components day];
    int month = (int)[components month];
    int year = (int)[components year];
    
    int baseCount = year*1000000 + month*10000 + day*100;
    
    
    if([userDefaults valueForKey:@"baseCount"] == nil){
        baseCount++;
        [userDefaults setValue:[[NSString alloc] initWithFormat:@"%d", baseCount] forKey:@"baseCount"];
        
        return NO;
        
    }else{
        
        long delta = ([[userDefaults valueForKey:@"baseCount"] intValue])-baseCount;
        
        if(delta< 0){
            
            [userDefaults setValue:[[NSString alloc] initWithFormat:@"%d", baseCount+1] forKey:@"baseCount"];
            
            return NO;
            
        }else if(delta < 10){
           // baseCount++;
            baseCount = [[userDefaults valueForKey:@"baseCount"] intValue]+1;
            [userDefaults setValue:[[NSString alloc] initWithFormat:@"%d", baseCount] forKey:@"baseCount"];
            NSLog(@"delta === %ld", delta);
            
            return NO;
        }else{
            
            return YES;
        }
    }
}

    



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainCell * cell = [tableView dequeueReusableCellWithIdentifier:@"MainCell"];
    
    if (cell == nil){
        cell = [[MainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainCell"];
    }
    
    cell.btnAboutandHelp.layer.borderWidth = 1;
    cell.btnAboutandHelp.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.btnAboutandHelp.layer.cornerRadius = 10;
    cell.btnSignup.layer.cornerRadius = 10;
    cell.btnQuestionMark1.layer.cornerRadius = 10;
    cell.btnQuestionMark2.layer.cornerRadius = 10;
    [cell.btnAboutandHelp setTitle:NSLocalizedString(@"About / Help", nil) forState:UIControlStateNormal];
    
    if(tvFollow.contentOffset.y > 180){
        
        self.followView.hidden = YES;
        topView.hidden =  NO;
        
        
    }else{
        
        self.followView.hidden =  NO;
        topView.hidden = YES;
    }

   
    
    NSString *name0 = [userDefaults valueForKey:[NSString stringWithFormat: @"nameTitle%@", [userDefaults valueForKey:@"setting0"]]];
    NSString *name1 = [userDefaults valueForKey:[NSString stringWithFormat: @"nameTitle%@", [userDefaults valueForKey:@"setting1"]]];
    NSString *name2 = [userDefaults valueForKey:[NSString stringWithFormat: @"nameTitle%@", [userDefaults valueForKey:@"setting2"]]];
    
    
    if(name0 != nil){
        
        [cell.btnSignup setTitle:name0 forState:UIControlStateNormal];
        //NSLog(@"name0====******%@",name0);
    }
    if(name1 != nil){
        
        [cell.btnQuestionMark1 setTitle:name1 forState:UIControlStateNormal];
        //NSLog(@"name1====******%@",name1);
    }
    
    if(name2 != nil){
        
        [cell.btnQuestionMark2 setTitle:name2 forState:UIControlStateNormal];
        //NSLog(@"name2====******%@",name2);
    }
    

    
        [cell.btnSignup addTarget:self action:@selector(gotoMainService:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnQuestionMark1 addTarget:self action:@selector(gotoMainService:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnQuestionMark2 addTarget:self action:@selector(gotoMainService:) forControlEvents:UIControlEventTouchUpInside];
        
       // cell.btnSignupSetting.tag = indexPath.row * 10;
        [cell.btnSignupSetting addTarget:self action:@selector(gotoSetting:) forControlEvents:UIControlEventTouchUpInside];


        [cell.btnQuestionSetting1 addTarget:self action:@selector(gotoSetting:) forControlEvents:UIControlEventTouchUpInside];

     
        [cell.btnQuestionSetting2 addTarget:self action:@selector(gotoSetting:) forControlEvents:UIControlEventTouchUpInside];
       /*
        cell.btnAboutandHelp.tag = indexPath.row;*/
    
        [cell.btnAboutandHelp addTarget:self action:@selector(gotoHelp:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView didEndScrolling:decelerate];
}

-(void)gotoMainService : (UIButton *) button{
    
    if (button.tag == 10) {
        buttonTag = 0;
    }else if (button.tag == 11){
        buttonTag = 1;
    }else{
        buttonTag = 2;
    }
  
        post = [userDefaults valueForKey:[NSString stringWithFormat:@"setting%ld", button.tag - 10]];
    
        //NSLog(@"post=====%@", post);
    
        if(post != nil){
            
            [self getData];
            
        }
        else{
            
        }
  
}
-(void) gotoHelp : (UIButton *)button{
    
    HelpViewController *viewCon = (HelpViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    
    [self.navigationController pushViewController:viewCon animated:YES];
}
-(void) gotoSetting : (UIButton *) button{
    
    
        SettingViewController *viewCon = (SettingViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
    
        viewCon.index1 = (int)button.tag-20;
    
        [self.navigationController pushViewController:viewCon animated:YES];
    
    
}

- (void) getData {
    
    
    if ([self getDataFromLocal:post].count > 1) {
        
        offlineFlag = YES;
        imageAllLoaded = 10;
        [self parseData:[self getDataFromLocal:post]];
        imageAllLoaded = 0;
     
        MainServiceViewController *viewCon = (MainServiceViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"MainServiceViewController"];
        [self.navigationController pushViewController:viewCon animated:YES];
      
    }else{
        offlineFlag = NO;
        imageAllLoaded = 0;
        [self getDataFromServer];
        
    }
}

-(NSDictionary *) getDataFromLocal : (NSString *)postID{
    
    NSString * valueKeyString = [NSString stringWithFormat:@"this%@", postID];
    NSDictionary * userinfo = (NSDictionary *)[userDefaults valueForKey:valueKeyString];
    
    return userinfo;
}

-(void) getDataFromServer{
    
    
    
    if ([self getConnectionCount]==YES) {
        
        [self showAlertDialog:@"" message:@"Can't connect to the server.\nPlease try tomorrow again." positive:@"OK" negative:nil sender:self];
    }else{
    
    [self showWhiteLoadingView];
        
        
  
        // Input
        NSString *originalString = post;
        
        // Intermediate
        NSString *numberString;
        
        NSScanner *scanner = [NSScanner scannerWithString:originalString];
        NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        
        // Throw away characters before the first number.
        [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
        
        // Collect numbers.
        [scanner scanCharactersFromSet:numbers intoString:&numberString];
        
        // Result.
       // int number = [numberString integerValue];
        NSString *postID = numberString;
        
        NSString * escapedUrl;
        
        if ([post hasPrefix:@"NL"]){
            
            NSString *escaped = @"https://www.guestinfo.org/nl/api12/userplus/get_post_meta/?key=5826fa6fd6aa4NL&post_id=";
            escapedUrl=[NSString stringWithFormat:@"%@%@",escaped ,postID];
        }else if([post hasPrefix:@"EN"]){
            
            NSString *escaped = @"https://www.guestinfo.org/en/api08/userplus/get_post_meta/?key=yeN5d589ghn8fEN&post_id=";
            escapedUrl=[NSString stringWithFormat:@"%@%@",escaped ,postID];
        }else if ([post hasPrefix:@"FR"]){
            NSString *escaped = @"https://www.guestinfo.org/fr/api13/userplus/get_post_meta/?key=zfN5d357gh9xfFR&post_id=";
            escapedUrl=[NSString stringWithFormat:@"%@%@",escaped ,postID];
        }else{
            [self showAlertDialog:@"" message:@"Invalid postID.\nPlease try again." positive:@"OK" negative:nil sender:self];
        }
    
    
    NSLog(@"REQ_ROGIN_URL %@",escapedUrl);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    
    NSURL *URL = [NSURL URLWithString:escapedUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        

        
        
        if (error) {
            
            [self hideLoadingView];
            
            [self showAlertDialog:@"" message:@"Connection to the server failed.\nPlease try again." positive:@"OK" negative:nil sender:self];
                serverIsConnected = NO;
            
        } else {
            
            NSString* result_code = [responseObject valueForKey:@"status"] ;
            
            //NSLog(@"LOGIN_RESULT_CODE: %@", result_code);
            
            if ([result_code isEqualToString: @"ok"]) {
                
                serverIsConnected = YES;
                
                
                NSDictionary * userinfo = [responseObject objectForKey:@"post_meta"];
                
                NSString *saveData = (NSString *)userinfo;
                //NSLog(@"saveData===%@",saveData);
                
                
                
                NSString * valueKeyString = [NSString stringWithFormat:@"this%@", post];
                
                [userDefaults setValue:saveData forKey:valueKeyString];
                
                
                userinfo = (NSDictionary *)[userDefaults valueForKey:valueKeyString];
                
                
                
                [self parseData:userinfo];
                
                if (serverIsConnected == YES) {
                    
                }else{
                    
                    [self getData];
                }
                
            } else {
                
                //NSLog(@"failed");
                
                
            }
        }
    }];
    
    [dataTask resume];
}
}


- (void) parseData: (NSDictionary *) userinfo{
    
    if(userinfo.count < 100){
        
        [self hideLoadingView];
        
        NSLog(@"failed for parse");
        [self showAlertDialog:@"" message:@"Please input valid id." positive:@"OK" negative:nil sender:self];
        
        return;
    }
  
   
    
    
    g_miscel = [userinfo valueForKey:@"miscel"];
    
    EmmergencyModel * emmergency;
    emmergency = [[EmmergencyModel alloc] init];
    emmergency = [EmmergencyModel parseEmmergency: &userinfo];
    g_emmergency = emmergency;
    
    AccommodationModel *accommodation;
    accommodation = [[AccommodationModel alloc] init];
    accommodation = [AccommodationModel parseAccommerdation:&userinfo];
    g_accommodation = accommodation;
    
    LocalContactPersonModel *localContactPerson;
    localContactPerson = [[LocalContactPersonModel alloc] init];
    localContactPerson = [LocalContactPersonModel parseLocalContactPerson:&userinfo];
    g_localContactPerson = localContactPerson;
    
    OwnerModel *owner;
    owner = [[OwnerModel alloc] init];
    owner = [OwnerModel parseOwner:&userinfo];
    g_owner = owner;
    
    FoodandDrinkModel *fd;
    fd = [[FoodandDrinkModel alloc] init];
    fd = [FoodandDrinkModel parseFoodandDrink : &userinfo];
    g_foodandDrink = fd;
    
    TrTaxiModel *trTaxi;
    trTaxi = [[TrTaxiModel alloc] init];
    trTaxi = [TrTaxiModel parseTrTaxi : &userinfo];
    g_trTaxi =trTaxi;
    
    TrCarModel *trCar;
    trCar = [[TrCarModel alloc] init];
    trCar = [TrCarModel parseTrCar: &userinfo];
    g_trCar = trCar;
    
    TrBikeModel *trBike;
    trBike = [[TrBikeModel alloc] init];
    trBike = [TrBikeModel parseTrBike: &userinfo];
    g_trBike = trBike;
    
    TrMotoModel *trMoto;
    trMoto = [[TrMotoModel alloc] init];
    trMoto = [TrMotoModel parseTrMoto: &userinfo];
    g_trMoto = trMoto;
    
    TrAirportModel *trAirport;
    trAirport = [[TrAirportModel alloc] init];
    trAirport = [TrAirportModel parseTrAirport:& userinfo];
    g_trAirport = trAirport;
    
    TrPublicModel *trPublic;
    trPublic = [[TrPublicModel alloc] init];
    trPublic = [TrPublicModel parseTrPublic:& userinfo];
    g_trPublic = trPublic;
    
    ComfortModel *comfort;
    comfort = [[ComfortModel alloc] init];
    comfort = [ComfortModel parseComfort:& userinfo];
    g_comfort = comfort;
    
    RbsModel *rsb;
    rsb = [[RbsModel alloc] init];
    rsb = [RbsModel parseRbs:& userinfo];
    g_rbs = rsb;
    
    EntertainmentModel *entertainment;
    entertainment = [[EntertainmentModel alloc] init];
    entertainment = [EntertainmentModel parseEntertainment:& userinfo];
    g_entertainment = entertainment;
    
    AttractionModel *attraction;
    attraction = [[AttractionModel alloc] init];
    attraction = [AttractionModel parseAttraction:&userinfo];
    g_attraction = attraction;
    
    ServicesModel *services;
    services = [[ServicesModel alloc] init];
    services = [ServicesModel parseServices:&userinfo];
    g_services= services;
    
    // set button name////////////
   
    NSString *name =  (NSString *)[userinfo valueForKey: @"name"];
    titleName = name;
    [userDefaults setValue:name forKey:[NSString stringWithFormat:@"nameTitle%@", post]];
    
    
    NSString *logoimagekeyword =  (NSString *)[userinfo valueForKey: @"a_logo"];
    [self getImageUrl:logoimagekeyword type:@"a_logo" ainType:post];
    logoimagekeyword =  [userinfo valueForKey: @"a_picture"];
    [self getImageUrl:logoimagekeyword type:@"a_picture" ainType:post];
    logoimagekeyword =  [userinfo valueForKey: @"attr_repeat_0_attr_picture"];
    [self getImageUrl:logoimagekeyword type:@"attr_repeat_0_attr_picture" ainType:post];
    logoimagekeyword =  [userinfo valueForKey: @"attr_repeat_1_attr_picture"];
    [self getImageUrl:logoimagekeyword type:@"attr_repeat_1_attr_picture" ainType:post];
    logoimagekeyword =  [userinfo valueForKey: @"attr_repeat_2_attr_picture"];
    [self getImageUrl:logoimagekeyword type:@"attr_repeat_2_attr_picture" ainType:post];
    logoimagekeyword =  [userinfo valueForKey: @"attr_repeat_3_attr_picture"];
    [self getImageUrl:logoimagekeyword type:@"attr_repeat_3_attr_picture" ainType:post];
    logoimagekeyword =  [userinfo valueForKey: @"attr_repeat_4_attr_picture"];
    [self getImageUrl:logoimagekeyword type:@"attr_repeat_4_attr_picture" ainType:post];
    
    [tblData reloadData];
    
}


- (void) getImageUrl: (NSString *) keyword type: (NSString *)imageType ainType:(NSString *)ain
{
    NSString * imageUrlKeyString = [NSString stringWithFormat:@"%@%@%@", imageType, keyword, ain];
    
    if(offlineFlag == NO){
        
        ////////server connect////
        if (keyword.length == 0)
            return;
        
        imageAllLoaded += 1;
        NSLog(@"%d" , imageAllLoaded);
        
         NSString * escapedUrl;
        
         if ([post hasPrefix:@"NL"]){
         
         
         escapedUrl =[NSString stringWithFormat:@"https://www.guestinfo.org/nl/api12/userplus/get_post_meta/?key=5826fa6fd6aa4NL&post_id=%@", keyword];
             
         }else if([post hasPrefix:@"EN"]){
         
         
         escapedUrl =[NSString stringWithFormat:@"https://www.guestinfo.org/en/api08/userplus/get_post_meta/?key=yeN5d589ghn8fEN&post_id=%@", keyword];
             
         }else if ([post hasPrefix:@"FR"]){
         
         escapedUrl =[NSString stringWithFormat:@"https://www.guestinfo.org/fr/api13/userplus/get_post_meta/?key=zfN5d357gh9xfFR&post_id=%@", keyword];
             
         }
        
        /* else{
         [self showAlertDialog:@"" message:@"Invalid postID.\nPlease try again." positive:@"OK" negative:nil sender:self];
         }*/

        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        
        
        NSURL *URL = [NSURL URLWithString:escapedUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        
        //NSLog(@"escapeUrl = %@", escapedUrl);
        //NSLog(@"request = %@", request);
        
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            
            imageAllLoaded -= 1;
            if (error) {
                
                //NSLog(@"Error: %@", error);
                
                
                
                NSString * urlString = [userDefaults valueForKey:imageUrlKeyString];
                
                if(urlString != nil){
                    
                    [self getImageUrlWithKey:imageUrlKeyString];
                    return ;
                }
                
                [self hideLoadingView];
                
                [self showAlertDialog:@"" message:@"Connection to the server failed.\nPlease try again." positive:@"OK" negative:nil sender:self];
                
            } else {
                
                NSString* result_code = [responseObject valueForKey:@"status"] ;
                
                //NSLog(@"LOGIN_RESULT_CODE: %@", result_code);
                
                if ([result_code isEqualToString: @"ok"]) {
                    
                    id userinfo = [responseObject objectForKey:@"post_meta"];
                    
                    NSString * logoString = [userinfo valueForKey: @"_wp_attached_file"];
                    
                    //NSLog(@"LOGOSTRING========%@", logoString);
                    
                    [self setImageUrlsWithKey:logoString type:imageType saveKey:imageUrlKeyString];
                    
                }
            }
        }];
        
        [dataTask resume];
    }
    else{
        
        ///////offline////////
        NSString *logoString = [userDefaults valueForKey:imageUrlKeyString];
        
        if ([post hasPrefix:@"NL"]) {
            logoString = [logoString stringByReplacingOccurrencesOfString:@"https://www.guestinfo.org/nl/files/" withString:@""];
        }else if ([post hasPrefix:@"FR"]){
            logoString = [logoString stringByReplacingOccurrencesOfString:@"https://www.guestinfo.org/fr/files/" withString:@""];
        }else{
            logoString = [logoString stringByReplacingOccurrencesOfString:@"https://www.guestinfo.org/en/files/" withString:@""];
        }
        
        
       
        [self setImageUrlsWithKey: logoString type:(NSString *)imageType saveKey:(NSString *) imageUrlKeyString];
        NSLog(@"ImageUrlKeyString===%@", imageUrlKeyString);
        NSLog(@"LogoString===%@", logoString);
    }
    
    
}

- (void) setImageUrlsWithKey: (NSString *) logoString type:(NSString *)imageType saveKey:(NSString *) imageUrlKeyString{
    
    //NSLog(@"ImageUrlKeyString===%@", imageUrlKeyString);
    //NSLog(@"LogoString===%@", logoString);
    NSString *languageKey;
    
    if ([post hasPrefix:@"NL"]) {
        languageKey = @"nl";
    }else if ([post hasPrefix:@"FR"]){
        languageKey = @"fr";
    }else{
        languageKey = @"en";
    }
    
    if ([imageType isEqualToString:@"a_logo"]){
        
    
        logoImageUrl = [NSString stringWithFormat:@"https://www.guestinfo.org/%@/files/%@", languageKey, logoString];
       
        [userDefaults setValue:logoImageUrl forKey:imageUrlKeyString];
        
        [imvLogo setImageWithURL:[NSURL URLWithString: logoImageUrl]];
        
    }
    else if([imageType isEqualToString:@"a_picture"]){
        categoryImageUrl = [NSString stringWithFormat:@"https://www.guestinfo.org/%@/files/%@", languageKey, logoString];
        [userDefaults setValue:categoryImageUrl forKey:imageUrlKeyString];
        
        [imvPicture setImageWithURL:[NSURL URLWithString: categoryImageUrl]];
    }
    else if([imageType isEqualToString:@"attr_repeat_0_attr_picture"]){
        g_attraction.attr_repeat_0_imageUrl = [NSString stringWithFormat:@"https://www.guestinfo.org/%@/files/%@", languageKey, logoString];
        [userDefaults setValue:g_attraction.attr_repeat_0_imageUrl forKey:imageUrlKeyString];
        NSLog(@"ATTRACTION.attr_repeat_0_imageUrl===%@", [userDefaults valueForKey:imageUrlKeyString]);
        [imvAttr0 setImageWithURL:[NSURL URLWithString: g_attraction.attr_repeat_0_imageUrl]];
    }
    else if([imageType isEqualToString:@"attr_repeat_1_attr_picture"]){
        g_attraction.attr_repeat_1_imageUrl = [NSString stringWithFormat:@"https://www.guestinfo.org/%@/files/%@", languageKey, logoString];
        [userDefaults setValue:g_attraction.attr_repeat_1_imageUrl forKey:imageUrlKeyString];
        NSLog(@"ATTRACTION.attr_repeat_1_imageUrl===%@", [userDefaults valueForKey:imageUrlKeyString]);
        
        [imvAttr1 setImageWithURL:[NSURL URLWithString: g_attraction.attr_repeat_1_imageUrl]];
    }
    else if([imageType isEqualToString:@"attr_repeat_2_attr_picture"]){
        g_attraction.attr_repeat_2_imageUrl = [NSString stringWithFormat:@"https://www.guestinfo.org/%@/files/%@", languageKey, logoString];
        [userDefaults setValue:g_attraction.attr_repeat_2_imageUrl forKey:imageUrlKeyString];
        NSLog(@"ATTRACTION.attr_repeat_2_imageUrl===%@", [userDefaults valueForKey:imageUrlKeyString]);
        [imvAttr2 setImageWithURL:[NSURL URLWithString: g_attraction.attr_repeat_2_imageUrl]];
    }
    else if([imageType isEqualToString:@"attr_repeat_3_attr_picture"]){
        g_attraction.attr_repeat_3_imageUrl = [NSString stringWithFormat:@"https://www.guestinfo.org/%@/files/%@", languageKey, logoString];
        [userDefaults setValue:g_attraction.attr_repeat_3_imageUrl forKey:imageUrlKeyString];
        
        [imvAttr3 setImageWithURL:[NSURL URLWithString: g_attraction.attr_repeat_3_imageUrl]];
    }
    else if([imageType isEqualToString:@"attr_repeat_4_attr_picture"]){
        g_attraction.attr_repeat_4_imageUrl = [NSString stringWithFormat:@"https://www.guestinfo.org/%@/files/%@", languageKey, logoString];
        [userDefaults setValue:g_attraction.attr_repeat_4_imageUrl forKey:imageUrlKeyString];
        
        [imvAttr4 setImageWithURL:[NSURL URLWithString: g_attraction.attr_repeat_4_imageUrl]];
    }
    
    
    NSLog(@"Minus === %d" , imageAllLoaded);

    if(imageAllLoaded == 0){
        
        [self hideLoadingView];
        
        MainServiceViewController *viewCon = (MainServiceViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"MainServiceViewController"];
        [self.navigationController pushViewController:viewCon animated:YES];
    }

    
    
    
}


- (void) getImageUrlWithKey: (NSString *) imageUrlKeyString{
    
    if ([imageUrlKeyString hasPrefix:@"a_logo"]){
        logoImageUrl = [userDefaults valueForKey:imageUrlKeyString];
        NSLog(@"Logo===%@", logoImageUrl);
    }
    else if([imageUrlKeyString hasPrefix:@"a_picture"]){
        categoryImageUrl = [userDefaults valueForKey:imageUrlKeyString];
        NSLog(@"%@", categoryImageUrl);
    }
    else if([imageUrlKeyString hasPrefix:@"attr_repeat_0_attr_picture"]){
        g_attraction.attr_repeat_0_imageUrl = [userDefaults valueForKey:imageUrlKeyString];
    }
    else if([imageUrlKeyString hasPrefix:@"attr_repeat_1_attr_picture"]){
        g_attraction.attr_repeat_1_imageUrl = [userDefaults valueForKey:imageUrlKeyString];
    }
    else if([imageUrlKeyString hasPrefix:@"attr_repeat_2_attr_picture"]){
        g_attraction.attr_repeat_2_imageUrl = [userDefaults valueForKey:imageUrlKeyString];
    }
    else if([imageUrlKeyString hasPrefix:@"attr_repeat_3_attr_picture"]){
        g_attraction.attr_repeat_3_imageUrl = [userDefaults valueForKey:imageUrlKeyString];
    }
    else if([imageUrlKeyString hasPrefix:@"attr_repeat_4_attr_picture"]){
        g_attraction.attr_repeat_4_imageUrl = [userDefaults valueForKey:imageUrlKeyString];
    }
}



@end
