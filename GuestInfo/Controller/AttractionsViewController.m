//
//  AttractionsViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/12/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "AttractionsViewController.h"
#import "MainMenuModel.h"
#import "CommonMenuViewController.h"
#import "Constants.h"
#import "AttractionModel.h"
#import "CommonUtils.h"

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;
extern NSString *titleName;
extern AttractionModel *g_attraction;

@interface AttractionsViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    NSMutableArray *attractionsArray;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
    
    NSString *attractionImageUrl;
}

@end

@implementation AttractionsViewController

@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, lblLogo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    topView.hidden = YES;
    
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    tvMainService.estimatedRowHeight = 50;
    attractionsArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
}

-(void) initView{
    
    //set logo image
    
    
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    
    lblLogo.text = NSLocalizedString(@"Attractions", nil);
    lblWelcome.text = titleName;
    
    for (int i=0;i< [g_attraction.attr_repeat intValue]; i++){
    
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        NSString *attractionString = [NSString stringWithFormat:@"attr_repeat_%d_attr_name", i];
        mainMenu.serviceName = [g_attraction valueForKey:attractionString];
        NSLog(@"keywords: %@, %@",attractionString, [g_attraction valueForKey:attractionString]);
        mainMenu.serviceIcon = @"ic_folder";
        
        [attractionsArray addObject:mainMenu];
        
    }
    
}
#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [g_attraction.attr_repeat intValue];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AttractionsCell * cell = [tableView dequeueReusableCellWithIdentifier:@"AttractionsCell"];
    
    if (cell == nil){
        cell = [[AttractionsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AttractionsCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) attractionsArray[indexPath.row];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
   
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) attractionsArray[indexPath.row];
    if(mainMenu.serviceName.length == 0){
        return 0;
    }
    return UITableViewAutomaticDimension;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    
    CommonMenuViewController *vc;
    vc = (CommonMenuViewController *)[self.storyboard instantiateViewControllerWithIdentifier: @"CommonMenuViewController"];
    
    vc.commonMenuArray = [[NSMutableArray alloc] init];
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    
    //Natural swimming pools
    
    if (row == 0) {
        mainMenu.serviceName = g_attraction.attr_repeat_0_attr_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
      
      //  mainMenu.serviceName = [CommonUtils convertHTML:g_attraction.attr_repeat_0_attr_descript];
        
        mainMenu.serviceName = g_attraction.attr_repeat_0_attr_descript;

        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_attraction.attr_repeat_0_attr_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_attraction.attr_repeat_0_attr_map.map_address;
        mainMenu.serviceLat = g_attraction.attr_repeat_0_attr_map.map_address_lat;
        mainMenu.serviceLng = g_attraction.attr_repeat_0_attr_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        attractionImageUrl = g_attraction.attr_repeat_0_imageUrl;
    }
    //Glass Bridge
    else if(row == 1){
        
        mainMenu.serviceName = g_attraction.attr_repeat_1_attr_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
       // mainMenu.serviceName = [CommonUtils convertHTML:g_attraction.attr_repeat_1_attr_descript];
        mainMenu.serviceName = g_attraction.attr_repeat_1_attr_descript;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_attraction.attr_repeat_1_attr_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_attraction.attr_repeat_1_attr_map.map_address;
        mainMenu.serviceLat = g_attraction.attr_repeat_1_attr_map.map_address_lat;
        mainMenu.serviceLng = g_attraction.attr_repeat_1_attr_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        attractionImageUrl = g_attraction.attr_repeat_1_imageUrl;
    }
    //Levada has...
    else if(row == 2){
        
        mainMenu.serviceName = g_attraction.attr_repeat_2_attr_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
        //mainMenu.serviceName = [CommonUtils convertHTML:g_attraction.attr_repeat_2_attr_descript];
        mainMenu.serviceName = g_attraction.attr_repeat_2_attr_descript;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_attraction.attr_repeat_2_attr_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
        attractionImageUrl = g_attraction.attr_repeat_2_imageUrl;
        
    
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_attraction.attr_repeat_2_attr_map.map_address;
        mainMenu.serviceLat = g_attraction.attr_repeat_2_attr_map.map_address_lat;
        mainMenu.serviceLng = g_attraction.attr_repeat_2_attr_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        attractionImageUrl = g_attraction.attr_repeat_2_imageUrl;
    }
    //Rabacal
    else if(row == 3){
        
        mainMenu.serviceName = g_attraction.attr_repeat_3_attr_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
       // mainMenu.serviceName = [CommonUtils convertHTML:g_attraction.attr_repeat_3_attr_descript];
        mainMenu.serviceName = g_attraction.attr_repeat_3_attr_descript;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_attraction.attr_repeat_3_attr_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_attraction.attr_repeat_3_attr_map.map_address;
        mainMenu.serviceLat = g_attraction.attr_repeat_3_attr_map.map_address_lat;
        mainMenu.serviceLng = g_attraction.attr_repeat_3_attr_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        attractionImageUrl = g_attraction.attr_repeat_3_imageUrl;
    }
    //Wandeling naar de Riberia Grande
    else if(row == 4){
        
        mainMenu.serviceName = g_attraction.attr_repeat_4_attr_name;
        [vc.commonMenuArray addObject:mainMenu];
        
        MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
       // mainMenu.serviceName = [CommonUtils convertHTML:g_attraction.attr_repeat_4_attr_descript];
        mainMenu.serviceName = g_attraction.attr_repeat_4_attr_descript;
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = WUT;
        mainMenu.serviceName = g_attraction.attr_repeat_4_attr_website;
        mainMenu.serviceIcon = @"ic_browser";
        [vc.commonMenuArray addObject:mainMenu];
        
        mainMenu = [[MainMenuModel alloc] init];
        mainMenu.serviceId = AT;
        mainMenu.serviceName = g_attraction.attr_repeat_4_attr_map.map_address;
        mainMenu.serviceLat = g_attraction.attr_repeat_4_attr_map.map_address_lat;
        mainMenu.serviceLng = g_attraction.attr_repeat_4_attr_map.map_address_lng;
        mainMenu.serviceIcon = @"ic_map";
        [vc.commonMenuArray addObject:mainMenu];
        attractionImageUrl = g_attraction.attr_repeat_4_imageUrl;
    }
    
    vc.logoText = @"Attractions";
    vc.imvTopBarName = @"ic_top_bar_image_attractions.png";
    //vc.lblWelcomeName = @"Casa Nipa B&B";
    vc.lblWelcomeName = titleName;
    vc.isAttration = YES;
    vc.attractionImageUrl = attractionImageUrl;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)onTappedBackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
