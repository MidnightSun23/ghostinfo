//
//  CommonUtils.h
//  GuestInfo
//
//  Created by MidnightSun on 12/18/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"
#import "LanguageModel.h"

@interface CommonUtils : NSObject

+ (AddressModel *)getAddress: (NSString *) addressString;

+ (NSString *) stringForAddress: (NSString *) rawString;

+(LanguageModel *) getLanguage: (NSString *) languageString;

+ (NSString *) stringForLanguage: (NSString *) rawString;

+(NSString *)convertHTML:(NSString *)html;

+(BOOL) isHtmlString : (NSString *) string;

@end
