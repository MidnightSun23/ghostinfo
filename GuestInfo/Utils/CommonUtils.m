//
//  CommonUtils.m
//  GuestInfo
//
//  Created by MidnightSun on 12/18/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "CommonUtils.h"

@implementation CommonUtils

+ (AddressModel *) getAddress: (NSString *) addressString{
    
    AddressModel * address = [[AddressModel alloc]init];
    
    NSArray *items = [addressString componentsSeparatedByString:@";"];
    
    if (items.count > 1){
        
        address.map_address = [CommonUtils stringForAddress: items[1]];
        address.map_address_lat = [CommonUtils stringForAddress: items[3]];
        address.map_address_lng = [CommonUtils stringForAddress: items[5]];
        
    }
    
    return address;
}


+ (NSString *) stringForAddress: (NSString *) rawString
{
    
    NSArray * items = [rawString componentsSeparatedByString:@"\""];
    if (items.count > 1){
        return items[1];
    }
    return @"";
}


+(LanguageModel *) getLanguage: (NSString *) languageString{
    
    LanguageModel *language = [[LanguageModel alloc] init];
    
    NSArray *items = [languageString componentsSeparatedByString:@";"];
    
    if (items.count > 1){
        
      for(int i = 1; i< items.count; i+=2){
          
            language.languageArray[(i-1)/2] = [CommonUtils stringForLanguage:items[i]];
        }
    }
    return language;
    
}
+ (NSString *) stringForLanguage: (NSString *) rawString
{
    
    NSArray * items = [rawString componentsSeparatedByString:@"\""];
    if (items.count > 1){
        
        return items[1];
    }
    return @"";
}
 

+(NSString *)convertHTML:(NSString *)html {
    
   /* NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];*/
    
    html = [NSString stringWithFormat:@"<center><font size = 5>%@%@", [html stringByReplacingOccurrencesOfString:@"\r\n" withString:@"<br>"], @"</font></center>"];
    
    return html;
}

+(BOOL) isHtmlString : (NSString *) string{
    if([string containsString:@"<"]){
        return YES;
    }
    return NO;
}


@end
