//
//  CommonMenuViewController.h
//  GuestInfo
//
//  Created by MidnightSun on 12/13/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonMenuCell.h"
#import "CommonViewController.h"
@import ScrollingFollowView;

@interface CommonMenuViewController : CommonViewController

@property (weak, nonatomic) IBOutlet ScrollingFollowView *followView1;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topFollowViewConstrain;

@property (weak, nonatomic) IBOutlet UITableView *tvMainService;
@property (weak, nonatomic) IBOutlet UIView *topView;

@property (strong, nonatomic) IBOutlet UIImageView *imvTopBar;
@property (strong) NSString *imvTopBarName;

@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;
@property (strong) NSString *lblWelcomeName;
@property (strong) NSMutableArray *commonMenuArray;

@property BOOL isAttration;
@property NSString *attractionImageUrl;
//@property (weak, nonatomic) IBOutlet UILabel *lblLogo;
@property (weak, nonatomic) IBOutlet UILabel *commonLblLogo;
@property (strong) NSString *logoText;

- (IBAction)onTappedBackBtn:(id)sender;



@end
