//
//  CommonLabelMenuViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/17/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "CommonLabelMenuViewController.h"
#import "MainMenuModel.h"
#import "WebViewController.h"
#import "Constants.h"

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;

@interface CommonLabelMenuViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate>{
    
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
    __weak IBOutlet NSLayoutConstraint *leftConstraint;
    
}

@end

@implementation CommonLabelMenuViewController


@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, commonMenuArray, imvTopBar, imvTopBarName, lblWelcomeName, lblLogo, logoText;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    topView.hidden = YES;
    
    imvTopBar.image = [UIImage imageNamed:imvTopBarName];
    lblWelcome.text = lblWelcomeName;
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    tvMainService.estimatedRowHeight = 50;
    
    //   [self initView];
    //set logo image
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    
    lblLogo.text = NSLocalizedString(logoText, nil);
    
    if(_isAttration){
        
        leftConstraint.constant = -[UIScreen mainScreen].bounds.size.width;
        [imvCategory setImageWithURL:[NSURL URLWithString:_attractionImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
        
    }
    else{
        [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
        
        [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    }
    
    NSLog(@"This is commonviewcontroller");
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [commonMenuArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CommonLabelMenuCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CommonLabelMenuCell"];
    
    if (cell == nil){
        cell = [[CommonLabelMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CommonLabelMenuCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) commonMenuArray[indexPath.row];
    
    cell.lblComment.text = [NSString stringWithFormat:@"%@", mainMenu.serviceComment];
    cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
    
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) commonMenuArray[indexPath.row];
    if(mainMenu.serviceName.length == 0){
        return 0;
    }
    return UITableViewAutomaticDimension;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) commonMenuArray [row];
    
    
    
    if ([mainMenu.serviceId isEqual: PT]) {
        
        NSLog(@"phoneTyope selected===%@",PT);
        NSString *phoneNumberString = mainMenu.serviceName;
        phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
        phoneNumberString = [NSString stringWithFormat:@"tel:%@", phoneNumberString];
        NSURL *phoneNumberURL = [NSURL URLWithString:phoneNumberString];
        [[UIApplication sharedApplication] openURL:phoneNumberURL];
        
        NSLog(@"PhoneNumber=======%@", phoneNumberString);
        
    }
    if ([mainMenu.serviceId isEqual:AT]) {
        
        NSString *address = [NSString stringWithFormat:@"%@",mainMenu.serviceName];
        NSString *lat = [NSString stringWithFormat:@"%@",mainMenu.serviceLat];
        NSString *lng = [NSString stringWithFormat:@"%@",mainMenu.serviceLng];
        
        NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?q=%@&center=%@,%@&zoom=14&views=traffic",
                         [address stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]],[lat stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]],[lng stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
        
        if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:@"comgooglemaps://"]]) {
            [[UIApplication sharedApplication] openURL:
             [NSURL URLWithString:url]];
        } else {
            
            [self showAlertDialog:@"" message:@"Can't use comgooglemaps! Please install the google map App" positive:@"OK" negative:nil sender:self];
            
            NSLog(@"Can't use comgooglemaps://");
            
            
        }
        
    }
    
    //Twitter link
    if ([mainMenu.serviceId isEqual:TUT]) {
  
        
        NSString * twitterURL = [NSString stringWithFormat:@"http://twitter.com/%@",[mainMenu.serviceName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:twitterURL]]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterURL]];
            
        }else{
            
            WebViewController *vcWeb;
            vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            
            vcWeb.webURL = [NSString stringWithFormat:@"http://twitter.com/%@",mainMenu.serviceName];
            NSLog(@"twitterlink=======%@", vcWeb.webURL );
            [self.navigationController pushViewController:vcWeb animated:YES];
            
        }
    }
    
    //Web site link
    if ([mainMenu.serviceId isEqual:WUT]){
        
        WebViewController *vcWeb;
        vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
        vcWeb.webURL = mainMenu.serviceName;
        [self.navigationController pushViewController:vcWeb animated:YES];
        
    }
    // facebook link
    
    if ([mainMenu.serviceId isEqual:FUT]){
       
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:mainMenu.serviceName]]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mainMenu.serviceName]];
            
        }else{
            
            WebViewController *vcWeb;
            vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            vcWeb.webURL = mainMenu.serviceName;
            [self.navigationController pushViewController:vcWeb animated:YES];
        }
     
    }
    
    if ([mainMenu.serviceId isEqual:FUT]){
        
        NSString *phoneNumberString = mainMenu.serviceName;
        phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
        phoneNumberString = [NSString stringWithFormat:@"tel:%@", phoneNumberString];
        NSURL *phoneNumberURL = [NSURL URLWithString:phoneNumberString];
        [[UIApplication sharedApplication] openURL:phoneNumberURL];
    }
    if ([mainMenu.serviceId isEqual:ET]){
        
        WebViewController *vcWeb;
        vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
        
        vcWeb.webURL = [NSString stringWithFormat:@"http://%@",mainMenu.serviceName];
        NSLog(@"twitterlink=======%@", vcWeb.webURL );
        [self.navigationController pushViewController:vcWeb animated:YES];
        
    }
        
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)onTappedBackBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



@end
