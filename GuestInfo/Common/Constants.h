//
//  Constants.h
//  GuestInfo
//
//  Created by MidnightSun on 12/15/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
@import AFNetworking;


@interface Constants : NSObject

#define PT @"phoneType"         //phoneNumber Type
#define AT @"addressType"       //Address Type
#define TUT @"twitterUrlType"   //Twitter Url Type
#define FUT @"facebookUrlType"  //Facebook Url Type
#define WUT @"WebsiteUrlType"   //WEbsite Url Type
#define SUT @"urlType"          //skype Url Type
#define ET  @"emailType"        //email Type


#define ENTERTAINMENT @"entertainment"

#define PRIMARYCOLOR ([UIColor colorWithRed:0.965f green:0.557f blue:0.337f alpha:1])



#define PROFILE_SIZE (CGSizeMake(256.0f, 256.0f))
#define IMAGE_SIZE (CGSizeMake(512.0f, 512.0f))

@end
