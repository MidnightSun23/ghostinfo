//
//  CommonViewController.h
//  GuestInfo
//
//  Created by MidnightSun on 12/11/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"

@interface CommonViewController : UIViewController{
    
    MBProgressHUD * HUD;
}



- (void) showWhiteLoadingView;
- (void) hideLoadingView;
- (void) showLoadingView;
- (void) hideLoadingView : (NSTimeInterval) delay;
- (void) showLoadingViewWithTitle:(NSString *) title sender:(id) sender;

- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative sender:(id) sender;


@end
