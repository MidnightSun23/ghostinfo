//
//  WebViewController.h
//  GuestInfo
//
//  Created by MidnightSun on 12/13/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *viewWeb;
@property (strong) NSString *webURL;
- (IBAction)onTappedBackBtn:(id)sender;

@end
