//
//  CommonMenuCell.h
//  GuestInfo
//
//  Created by MidnightSun on 12/13/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonMenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblCell;
@property (weak, nonatomic) IBOutlet UIImageView *imvCell;
@property (weak, nonatomic) IBOutlet UIView *cellView;
@property (weak, nonatomic) IBOutlet UIButton *btnCell;

@end
