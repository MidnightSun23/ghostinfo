//
//  CommonMenuViewController.m
//  GuestInfo
//
//  Created by MidnightSun on 12/13/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "CommonMenuViewController.h"
#import "MainMenuModel.h"
#import "WebViewController.h"
#import "Constants.h"
#import "CommonUtils.h"
#import <MessageUI/MessageUI.h>

extern NSString * logoImageUrl;
extern NSString * categoryImageUrl;

@interface CommonMenuViewController ()<UITableViewDelegate,UITableViewDelegate,UIScrollViewDelegate, MFMailComposeViewControllerDelegate>{
    
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UIImageView *imvCategory;
    __weak IBOutlet NSLayoutConstraint *leftConstraint;

}

@end

@implementation CommonMenuViewController


@synthesize followView1, topFollowViewConstrain, tvMainService, topView, lblWelcome, commonMenuArray, imvTopBar, imvTopBarName, lblWelcomeName, commonLblLogo, logoText, attractionImageUrl;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"LogoText===%@",logoText);
    topView.hidden = YES;
    
    imvTopBar.image = [UIImage imageNamed:imvTopBarName];
    lblWelcome.text = lblWelcomeName;
    CGFloat scrollingFollowViewHeight = followView1.frame.size.height;
    tvMainService.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [followView1 setupWithConstraint:topFollowViewConstrain maxFollowPoint:scrollingFollowViewHeight minFollowPoint:0 allowHalfDisplay:NO];
    tvMainService.estimatedRowHeight = 50;
    
 //   [self initView];
    //set logo image
    
    [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
    
    [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    
    commonLblLogo.text = NSLocalizedString(logoText, nil);
    
    
    if(_isAttration){
        
        leftConstraint.constant = -[UIScreen mainScreen].bounds.size.width;
        [imvCategory setImageWithURL:[NSURL URLWithString:attractionImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
        NSLog(@"attractionImageUrl === %@", attractionImageUrl );
    }
    else{
        [imvLogo setImageWithURL:[NSURL URLWithString:logoImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyLogo"]];
        
        [imvCategory setImageWithURL:[NSURL URLWithString:categoryImageUrl] placeholderImage:[UIImage imageNamed:@"ic_emptyCategory"]];
    }
    
    NSLog(@"This is CommonMenuviewcontroller");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [followView1 didScroll:scrollView];
}
-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [followView1 didEndScrolling:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [followView1 didEndScrolling:decelerate];
}

#pragma mark - TableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [commonMenuArray count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CommonMenuCell * cell = [tableView dequeueReusableCellWithIdentifier:@"CommonMenuCell"];
    
    if (cell == nil){
        cell = [[CommonMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CommonMenuCell"];
    }
    
    cell.cellView.layer.cornerRadius = 7.0;
    
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) commonMenuArray[indexPath.row];
    
    if([CommonUtils isHtmlString: mainMenu.serviceName])
    {
        NSString *htmlString = [CommonUtils convertHTML:mainMenu.serviceName];
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        cell.lblCell.attributedText = attributedString;
    }
    else{
        cell.lblCell.text = [NSString stringWithFormat:@"%@", mainMenu.serviceName];
    }
    
    cell.imvCell.image = [UIImage imageNamed:mainMenu.serviceIcon];
 
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) commonMenuArray[indexPath.row];
    if(mainMenu.serviceName.length == 0){
        return 0;
    }
    return UITableViewAutomaticDimension;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    MainMenuModel *mainMenu = [[MainMenuModel alloc] init];
    mainMenu = (MainMenuModel *) commonMenuArray [row];
    
    
 
    if ([mainMenu.serviceId isEqual: PT]) {
        
            NSLog(@"phoneTyope selected===%@",PT);
            NSString *phoneNumberString = (NSString *)mainMenu.serviceName;
            phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
            phoneNumberString = [NSString stringWithFormat:@"tel:%@", phoneNumberString];
            NSURL *phoneNumberURL = [NSURL URLWithString:phoneNumberString];
            [[UIApplication sharedApplication] openURL:phoneNumberURL];
            
            NSLog(@"PhoneNumber=======%@", phoneNumberString);
      
    }
    if ([mainMenu.serviceId isEqual:AT]) {
        
     /*
        WebViewController *vcWeb;
        vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
   
        //NSString *latlong = @"-56.568545,1.256281";
        NSString *latlong = [NSString stringWithFormat:@"%@,%@",mainMenu.serviceLat,mainMenu.serviceLng];
        NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?q=loc:%@%@(%@)",
                         [latlong stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]],[@" " stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]],[mainMenu.serviceName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
        // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        
        vcWeb.webURL = url;
        [self.navigationController pushViewController:vcWeb animated:YES];
      */
        NSString *address = [NSString stringWithFormat:@"%@",mainMenu.serviceName];
        NSString *lat = [NSString stringWithFormat:@"%@",mainMenu.serviceLat];
        NSString *lng = [NSString stringWithFormat:@"%@",mainMenu.serviceLng];
        
        NSString *url = [NSString stringWithFormat: @"http://maps.google.com/maps?q=%@&center=%@,%@&zoom=14&views=traffic",
                         [address stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]],[lat stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]],[lng stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
        
        if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:@"comgooglemaps://"]]) {
            [[UIApplication sharedApplication] openURL:
             [NSURL URLWithString:url]];
        } else {
            
            [self showAlertDialog:@"" message:@"Can't use comgooglemaps! Please install the google map App" positive:@"OK" negative:nil sender:self];
            
            NSLog(@"Can't use comgooglemaps://");
            
            
        }

    }
    
    //Twitter link
    if ([mainMenu.serviceId isEqual:TUT]) {
        

        NSString * twitterURL = [NSString stringWithFormat:@"http://twitter.com/%@",[mainMenu.serviceName stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
        
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:twitterURL]]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterURL]];
            
        }else{
            
            WebViewController *vcWeb;
            vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            
            vcWeb.webURL = [NSString stringWithFormat:@"http://twitter.com/%@",mainMenu.serviceName];
            NSLog(@"twitterlink=======%@", vcWeb.webURL );
            [self.navigationController pushViewController:vcWeb animated:YES];
            
        }
    }
    
    //Web site link
    if ([mainMenu.serviceId isEqual:WUT]){
        
        WebViewController *vcWeb;
        vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
        vcWeb.webURL = mainMenu.serviceName;
        [self.navigationController pushViewController:vcWeb animated:YES];
    }
    // facebook link
    if ([mainMenu.serviceId isEqual:FUT]){
               
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:mainMenu.serviceName]]) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mainMenu.serviceName]];
            
        }else{
            
            WebViewController *vcWeb;
            vcWeb= (WebViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
            vcWeb.webURL = mainMenu.serviceName;
            [self.navigationController pushViewController:vcWeb animated:YES];
        }
    }
    
    if ([mainMenu.serviceId isEqual:FUT]){
        
        NSString *phoneNumberString = mainMenu.serviceName;
        phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
        phoneNumberString = [NSString stringWithFormat:@"tel:%@", phoneNumberString];
        NSURL *phoneNumberURL = [NSURL URLWithString:phoneNumberString];
        [[UIApplication sharedApplication] openURL:phoneNumberURL];
    }
    if ([mainMenu.serviceId isEqual:ET]){
               if ([MFMailComposeViewController canSendMail])
         {
             // Email Subject
             NSString *emailTitle = @"";
             // Email Content
             NSString *messageBody = @"";
             // To address
             NSArray *toRecipents = [NSArray arrayWithObject:mainMenu.serviceName];
             
             MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
             mc.mailComposeDelegate = self;
             [mc setSubject:emailTitle];
             [mc setMessageBody:messageBody isHTML:NO];
             [mc setToRecipients:toRecipents];
             
             // Present mail view controller on screen
             [self presentViewController:mc animated:YES completion:NULL];
         }
         else
         {
             NSLog(@"This device cannot send email");
         }

        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onTappedBackBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}



@end
