//
//  MainCell.h
//  GuestInfo
//
//  Created by MidnightSun on 12/11/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnSignup;
@property (weak, nonatomic) IBOutlet UIButton *btnQuestionMark1;
@property (weak, nonatomic) IBOutlet UIButton *btnQuestionMark2;
@property (weak, nonatomic) IBOutlet UIButton *btnAboutandHelp;

@property (weak, nonatomic) IBOutlet UIButton *btnSignupSetting;
@property (weak, nonatomic) IBOutlet UIButton *btnQuestionSetting1;

@property (weak, nonatomic) IBOutlet UIButton *btnQuestionSetting2;


@end
