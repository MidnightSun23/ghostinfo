//
//  AppDelegate.m
//  GuestInfo
//
//  Created by MidnightSun on 12/7/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "AppDelegate.h"
@import GoogleMaps;
@import GooglePlaces;


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [GMSServices provideAPIKey:@"AIzaSyBxrEhu8NsS-Dhc7NNOtzZxZaObBFkm7QI"];
    
    [GMSPlacesClient provideAPIKey:@"AIzaSyBxrEhu8NsS-Dhc7NNOtzZxZaObBFkm7QI"];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
  
    
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    
    NSString *preLanguageCode = [userDefaults valueForKey:@"settingLanguageCode"];
    NSLog(@"LanguageCode===%@", languageCode);
    NSLog(@"PreLanguageCode===%@", preLanguageCode);
    
    if ((![preLanguageCode  isEqual: @""])&&(![languageCode isEqualToString:preLanguageCode])){
        
        if ([languageCode isEqualToString:@"en"]) {
            
            [userDefaults setValue:@"EN228" forKey:@"setting0"];
            [userDefaults setValue:languageCode forKey:@"settingLanguageCode"];
        }else if([languageCode isEqualToString:@"nl"]){
            [userDefaults setValue:@"NL350" forKey:@"setting0"];
            [userDefaults setValue:languageCode forKey:@"settingLanguageCode"];
        }else if([languageCode isEqualToString:@"fr"]){
            [userDefaults setValue:@"FR228" forKey:@"setting0"];
            [userDefaults setValue:languageCode forKey:@"settingLanguageCode"];
        }else{
            [userDefaults setValue:@"EN228" forKey:@"setting0"];
            [userDefaults setValue:languageCode forKey:@"settingLanguageCode"];
        }
        
    }
    
   ///////////////////////////////
    if([userDefaults valueForKey:@"setting0"] == nil){
        
    
    if ([languageCode isEqualToString:@"en"]) {
        
        [userDefaults setValue:@"EN228" forKey:@"setting0"];
        [userDefaults setValue:languageCode forKey:@"settingLanguageCode"];
        
    }else if([languageCode isEqualToString:@"nl"]){
        
        [userDefaults setValue:@"NL350" forKey:@"setting0"];
        [userDefaults setValue:languageCode forKey:@"settingLanguageCode"];
        
    }else if([languageCode isEqualToString:@"fr"]){
        
        [userDefaults setValue:@"FR228" forKey:@"setting0"];
        [userDefaults setValue:languageCode forKey:@"settingLanguageCode"];
       
    }else{
        
        [userDefaults setValue:@"EN228" forKey:@"setting0"];
        [userDefaults setValue:languageCode forKey:@"settingLanguageCode"];
       
    }
    }
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
