//
//  TrMotoModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface TrMotoModel : NSObject


@property (strong) NSString *trm_name;
@property (strong) NSString *trm_phone;
@property (strong) NSString *trm_website;
@property (strong) NSString *trm_email;
@property (strong) AddressModel *trm_address;

- (instancetype) init;
+ (TrMotoModel *) parseTrMoto: (id *) rawData;

@end
