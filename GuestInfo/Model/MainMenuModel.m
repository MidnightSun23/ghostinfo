//
//  MainMenuModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/11/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "MainMenuModel.h"

@implementation MainMenuModel

@synthesize serviceId, serviceName, serviceIcon, serviceImage, serviceLat, servicetown, serviceCountry, serviceLng,serviceComment;

-(id) init{
    if(self == [super init])
    {
        serviceId =  @"";
        serviceName = @"";
        serviceIcon = @"";
        serviceImage = @"";
        
        serviceLat = @"";
        serviceLng = @"";
        serviceCountry = @"";
        servicetown = @"";
        serviceComment = @"";
        
    }
    
    return self;
}

@end
