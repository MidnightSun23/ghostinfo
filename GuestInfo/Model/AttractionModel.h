//
//  AttractionModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/16/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface AttractionModel : NSObject

@property (strong) NSString *attr_repeat;
@property (strong) NSString *attr_repeat_0_attr_name;
@property (strong) NSString *attr_repeat_0_attr_website;
@property (strong) NSString *attr_repeat_0_attr_picture;
@property (strong) NSString *attr_repeat_0_attr_descript;
@property (strong) AddressModel *attr_repeat_0_attr_map;
@property (strong) NSString *attr_repeat_0_imageUrl;

@property (strong) NSString *attr_repeat_1_attr_name;
@property (strong) NSString *attr_repeat_1_attr_website;
@property (strong) NSString *attr_repeat_1_attr_picture;
@property (strong) NSString *attr_repeat_1_attr_descript;
@property (strong) AddressModel *attr_repeat_1_attr_map;

@property (strong) NSString *attr_repeat_1_imageUrl;

@property (strong) NSString *attr_repeat_2_attr_name;
@property (strong) NSString *attr_repeat_2_attr_website;
@property (strong) NSString *attr_repeat_2_attr_picture;
@property (strong) NSString *attr_repeat_2_attr_descript;
@property (strong) AddressModel *attr_repeat_2_attr_map;

@property (strong) NSString *attr_repeat_2_imageUrl;

@property (strong) NSString *attr_repeat_3_attr_name;
@property (strong) NSString *attr_repeat_3_attr_website;
@property (strong) NSString *attr_repeat_3_attr_picture;
@property (strong) NSString *attr_repeat_3_attr_descript;
@property (strong) AddressModel *attr_repeat_3_attr_map;

@property (strong) NSString *attr_repeat_3_imageUrl;

@property (strong) NSString *attr_repeat_4_attr_name;
@property (strong) NSString *attr_repeat_4_attr_website;
@property (strong) NSString *attr_repeat_4_attr_picture;
@property (strong) NSString *attr_repeat_4_attr_descript;
@property (strong) AddressModel *attr_repeat_4_attr_map;

@property (strong) NSString *attr_repeat_4_imageUrl;

- (instancetype) init;
+ (AttractionModel *) parseAttraction: (id *) rawData;

@end
