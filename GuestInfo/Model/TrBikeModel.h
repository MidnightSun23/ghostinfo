//
//  TrBikeModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface TrBikeModel : NSObject

@property (strong) NSString *trb_name;
@property (strong) NSString *trb_phone;
@property (strong) NSString *trb_website;
@property (strong) NSString *trb_email;
@property (strong) AddressModel *trb_address;

- (instancetype) init;
+ (TrBikeModel *) parseTrBike: (id *) rawData;

@end
