//
//  AccommodationModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "AccommodationModel.h"
#import "CommonUtils.h"

AccommodationModel *g_accommodation;

@implementation AccommodationModel

@synthesize a_welcome;
@synthesize a_logo;
@synthesize a_picture;

@synthesize a_address;
@synthesize a_zip;
@synthesize a_town;
@synthesize a_country;
@synthesize a_map;

@synthesize a_website;
@synthesize a_telephone;
@synthesize a_facebook;
@synthesize a_twitter;

-(instancetype) init{
 a_welcome = @"";
 a_logo= @"";
 a_picture= @"";
 a_address= @"";
 a_zip= @"";
 a_town= @"";
 a_country= @"";
 a_website= @"";
 a_telephone= @"";
 a_facebook= @"";
 a_twitter= @"";
 a_map = [[AddressModel alloc] init];
    
    return self;
}
+ (AccommodationModel *) parseAccommerdation: (id *) rawData{
    
    
        AccommodationModel *accommodation = [[AccommodationModel alloc] init];
        accommodation.a_welcome = [*rawData valueForKey:@"a_welcome"];
        accommodation.a_logo = [*rawData valueForKey:@"a_logo"];
        accommodation.a_picture = [*rawData valueForKey:@"a_picture"];
        accommodation.a_address = [*rawData valueForKey:@"a_address"];
        accommodation.a_zip = [*rawData valueForKey:@"a_zip"];
        accommodation.a_town = [*rawData valueForKey:@"a_town"];
        accommodation.a_country = [*rawData valueForKey:@"a_country"];
        accommodation.a_website = [*rawData valueForKey:@"a_website"];
        accommodation.a_telephone = [*rawData valueForKey:@"a_telephone"];
        accommodation.a_facebook = [*rawData valueForKey:@"a_facebook"];
        accommodation.a_twitter = [*rawData valueForKey:@"a_twitter"];
        
        NSString *a_map_string = [*rawData valueForKey:@"a_map"];
        accommodation.a_map = [CommonUtils getAddress: a_map_string];
        
        
        
        return accommodation;
    
}

@end
