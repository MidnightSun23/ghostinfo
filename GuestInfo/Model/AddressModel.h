//
//  AddressModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressModel : NSObject

@property (strong) NSString *map_address;
@property (strong) NSString *map_address_lat;
@property (strong) NSString *map_address_lng;

-(instancetype) init;

@end
