//
//  AccommodationModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface AccommodationModel : NSObject

@property (strong) NSString *a_welcome;
@property (strong) NSString *a_logo;
@property (strong) NSString *a_picture;
 
@property (strong) NSString *a_address;
@property (strong) NSString *a_zip;
@property (strong) NSString *a_town;
@property (strong) NSString *a_country;
@property (strong) AddressModel* a_map;

@property (strong) NSString *a_website;
@property (strong) NSString *a_telephone;
@property (strong) NSString *a_facebook;
@property (strong) NSString *a_twitter;

- (instancetype) init;
+ (AccommodationModel *) parseAccommerdation: (id *) rawData;

@end
