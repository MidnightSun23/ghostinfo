//
//  LanguageModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/25/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageModel : NSObject

@property (strong) NSMutableArray *languageArray;

-(instancetype) init;


@end
