//
//  MainMenuModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/11/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainMenuModel : NSObject

@property (strong) NSString *serviceId;
@property (strong) NSString *serviceName;
@property (strong) NSString *serviceIcon;
@property (strong) NSString *serviceImage;
@property (strong) NSString *serviceComment;


@property (strong) NSString *serviceLat;
@property (strong) NSString *serviceLng;
@property (strong) NSString *serviceCountry;
@property (strong) NSString *servicetown;

-(id) init;

@end
