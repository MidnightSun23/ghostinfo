//
//  EmmergencyModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/8/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface EmmergencyModel : NSObject

@property (strong) NSString *em_general;
@property (strong) NSString *em_fire;
@property (strong) NSString *em_ambulance;
@property (strong) NSString *em_police;
@property (strong) NSString *em_assistance;
@property (strong) NSString *em_docter_name;
@property (strong) NSString *em_docter_telephone;
@property (strong) AddressModel *em_docter_address;
@property (strong) AddressModel *em_pharmacy_address;

@property (strong) NSString *em_pharmacy_name;
@property (strong) NSString *em_pharmacy_telephone;
@property (strong) NSString *em_pharmacy_address_address;
@property (strong) NSString *em_pharmacy_address_lat;
@property (strong) NSString *em_pharmacy_address_lng;

@property (strong) NSString *imageUrl;

- (instancetype) init;
+ (EmmergencyModel *) parseEmmergency: (id *) rawData;

@end
