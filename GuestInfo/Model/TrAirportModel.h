//
//  TrAirportModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface TrAirportModel : NSObject

@property (strong) NSString *trair_name;
@property (strong) NSString *trair_phone;
@property (strong) NSString *trair_website;

@property (strong) AddressModel *trair_address;

- (instancetype) init;
+ (TrAirportModel *) parseTrAirport: (id *) rawData;

@end
