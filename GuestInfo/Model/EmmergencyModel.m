//
//  EmmergencyModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/8/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "EmmergencyModel.h"
#import "CommonUtils.h"


EmmergencyModel *g_emmergency;

@implementation EmmergencyModel

@synthesize em_general;
@synthesize em_fire;
@synthesize em_ambulance;
@synthesize em_police;
@synthesize em_assistance;
@synthesize em_docter_name;
@synthesize em_docter_telephone;
@synthesize em_docter_address;
@synthesize em_pharmacy_name;
@synthesize em_pharmacy_telephone;
@synthesize em_pharmacy_address;
@synthesize em_pharmacy_address_address;
@synthesize em_pharmacy_address_lat;
@synthesize em_pharmacy_address_lng;

- (instancetype) init{
    em_general= @"";
    em_fire= @"";
    em_ambulance= @"";
    em_police= @"";
    em_assistance= @"";
    em_docter_name= @"";
    em_docter_telephone= @"";
//    em_docter_address= @"";
//    em_docter_address_address= @"";
//    em_docter_address_lat= @"";
//    em_docter_address_lng= @"";
    em_pharmacy_name= @"";
    em_pharmacy_telephone= @"";
    em_pharmacy_address_address= @"";
    em_pharmacy_address_lat= @"";
    em_pharmacy_address_lng= @"";
    em_docter_address =[[AddressModel alloc] init];
    em_pharmacy_address =[[AddressModel alloc] init];
    
    return self;
}

+ (EmmergencyModel *) parseEmmergency: (id *) rawData
{
    EmmergencyModel * emmergency = [[EmmergencyModel alloc] init];
    
    emmergency.imageUrl = [*rawData valueForKey: @"em_general"];
    emmergency.em_general = [*rawData valueForKey: @"em_general"];
    emmergency.em_fire = [*rawData valueForKey: @"em_fire"];
    emmergency.em_ambulance = [*rawData valueForKey: @"em_ambulance"];
    emmergency.em_police = [*rawData valueForKey: @"em_police"];
    emmergency.em_assistance = [*rawData valueForKey: @"em_assistance"];
    emmergency.em_docter_name = [*rawData valueForKey: @"em_docter_name"];
    emmergency.em_docter_telephone = [*rawData valueForKey: @"em_docter_telephone"];

    emmergency.em_pharmacy_name = [*rawData valueForKey: @"em_pharmacy_name"];
    emmergency.em_pharmacy_telephone = [*rawData valueForKey: @"em_pharmacy_telephone"];

    
    //em_docter_address
    NSString *em_docter_address_string = [*rawData valueForKey:@"em_docter_address"];
    
    emmergency.em_docter_address = [CommonUtils getAddress: em_docter_address_string];
    
  //em_pharmacy_address
    NSString *em_pharmacy_address_string = [*rawData valueForKey:@"em_pharmacy_address"];
    
    emmergency.em_pharmacy_address = [CommonUtils getAddress: em_pharmacy_address_string];
    
       
    return emmergency;
}



@end
