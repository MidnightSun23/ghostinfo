//
//  TrAirportModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "TrAirportModel.h"
#import "CommonUtils.h"

TrAirportModel *g_trAirport;

@implementation TrAirportModel

@synthesize  trair_name;
@synthesize  trair_phone;
@synthesize  trair_website;
@synthesize  trair_address;

- (instancetype) init{
    
    trair_name = @"";
    trair_phone = @"";
    trair_website = @"";
    trair_address = [[AddressModel alloc] init];
    
    return self;
    
}
+ (TrAirportModel *) parseTrAirport: (id *) rawData{
    
    TrAirportModel *trAirport = [[TrAirportModel alloc] init];
    trAirport.trair_name = [*rawData valueForKey:@"trair_name"];
    trAirport.trair_phone = [*rawData valueForKey:@"trair_phone"];
    trAirport.trair_website = [*rawData valueForKey:@"trair_website"];
    
    //trair_address
    
        NSString *trair_address_string = [*rawData valueForKey:@"trair_address"];
    trAirport.trair_address = [CommonUtils getAddress: trair_address_string];
    
        return trAirport;
}

@end
