//
//  TrCarModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface TrCarModel : NSObject


@property (strong) NSString *trc_name;
@property (strong) NSString *trc_phone;
@property (strong) NSString *trc_website;
@property (strong) NSString *trc_email;
@property (strong) AddressModel *trc_address;

- (instancetype) init;
+ (TrCarModel *) parseTrCar: (id *) rawData;

@end
