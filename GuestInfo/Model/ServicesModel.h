//
//  ServicesModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/17/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface ServicesModel : NSObject

@property (strong) NSString *ser_checkintime;
@property (strong) NSString *ser_checkouttime;
@property (strong) NSString *ser_atm_address;
@property (strong) NSString *ser_wifi_pass;
@property (strong) NSString *ser_wifi_details;
@property (strong) NSString *ser_park_details;
@property (strong) NSString *ser_tv_details;


@property (strong) AddressModel *ser_atm_location;
@property (strong) AddressModel *ser_park_map;



- (instancetype) init;
+ (ServicesModel *) parseServices: (id *) rawData;



@end
