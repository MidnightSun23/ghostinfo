//
//  AttractionModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/16/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "AttractionModel.h"
#import "Constants.h"
#import "CommonUtils.h"

AttractionModel *g_attraction;

@implementation AttractionModel

@synthesize attr_repeat;
@synthesize attr_repeat_0_attr_name;
@synthesize attr_repeat_0_attr_website;
@synthesize attr_repeat_0_attr_picture;
@synthesize attr_repeat_0_attr_descript;
@synthesize attr_repeat_0_attr_map;

@synthesize attr_repeat_1_attr_name;
@synthesize attr_repeat_1_attr_website;
@synthesize attr_repeat_1_attr_picture;
@synthesize attr_repeat_1_attr_descript;
@synthesize attr_repeat_1_attr_map;

@synthesize attr_repeat_2_attr_name;
@synthesize attr_repeat_2_attr_website;
@synthesize attr_repeat_2_attr_picture;
@synthesize attr_repeat_2_attr_descript;
@synthesize attr_repeat_2_attr_map;

@synthesize attr_repeat_3_attr_name;
@synthesize attr_repeat_3_attr_website;
@synthesize attr_repeat_3_attr_picture;
@synthesize attr_repeat_3_attr_descript;
@synthesize attr_repeat_3_attr_map;

@synthesize attr_repeat_4_attr_name;
@synthesize attr_repeat_4_attr_website;
@synthesize attr_repeat_4_attr_picture;
@synthesize attr_repeat_4_attr_descript;
@synthesize attr_repeat_4_attr_map;


- (instancetype) init{
    
 attr_repeat = @"";
 attr_repeat_0_attr_name= @"";
 attr_repeat_0_attr_website= @"";
 attr_repeat_0_attr_picture= @"";
 attr_repeat_0_attr_descript= @"";
    
 attr_repeat_1_attr_name= @"";
 attr_repeat_1_attr_website= @"";
 attr_repeat_1_attr_picture= @"";
 attr_repeat_1_attr_descript= @"";
    
 attr_repeat_2_attr_name= @"";
 attr_repeat_2_attr_website= @"";
 attr_repeat_2_attr_picture= @"";
 attr_repeat_2_attr_descript= @"";
    
 attr_repeat_3_attr_name= @"";
 attr_repeat_3_attr_website= @"";
 attr_repeat_3_attr_picture= @"";
 attr_repeat_3_attr_descript= @"";
    
    attr_repeat_4_attr_name= @"";
    attr_repeat_4_attr_website= @"";
    attr_repeat_4_attr_picture= @"";
    attr_repeat_4_attr_descript= @"";
    
    attr_repeat_0_attr_map= [[AddressModel alloc] init];
    attr_repeat_1_attr_map= [[AddressModel alloc] init];
    attr_repeat_2_attr_map= [[AddressModel alloc] init];
    attr_repeat_3_attr_map= [[AddressModel alloc] init];
    attr_repeat_4_attr_map= [[AddressModel alloc] init];
    
    return  self;
}
+ (AttractionModel *) parseAttraction: (id *) rawData{
    
          AttractionModel *attraction = [[AttractionModel alloc] init];
        
          attraction.attr_repeat = [*rawData valueForKey:@"attr_repeat"];
          attraction.attr_repeat_0_attr_name = [*rawData valueForKey:@"attr_repeat_0_attr_name"];
          attraction.attr_repeat_0_attr_website = [*rawData valueForKey:@"attr_repeat_0_attr_website"];
          attraction.attr_repeat_0_attr_picture = [*rawData valueForKey:@"attr_repeat_0_attr_picture"];
          attraction.attr_repeat_0_attr_descript = [*rawData valueForKey:@"attr_repeat_0_attr_descript"];
          attraction.attr_repeat_1_attr_name = [*rawData valueForKey:@"attr_repeat_1_attr_name"];
          attraction.attr_repeat_1_attr_website = [*rawData valueForKey:@"attr_repeat_1_attr_website"];
          attraction.attr_repeat_1_attr_picture = [*rawData valueForKey:@"attr_repeat_1_attr_picture"];
          attraction.attr_repeat_1_attr_descript = [*rawData valueForKey:@"attr_repeat_1_attr_descript"];
          attraction.attr_repeat_2_attr_name = [*rawData valueForKey:@"attr_repeat_2_attr_name"];
          attraction.attr_repeat_2_attr_website = [*rawData valueForKey:@"attr_repeat_2_attr_website"];
          attraction.attr_repeat_2_attr_picture = [*rawData valueForKey:@"attr_repeat_2_attr_picture"];
          attraction.attr_repeat_2_attr_descript = [*rawData valueForKey:@"attr_repeat_2_attr_descript"];
          attraction.attr_repeat_3_attr_name = [*rawData valueForKey:@"attr_repeat_3_attr_name"];
          attraction.attr_repeat_3_attr_website = [*rawData valueForKey:@"attr_repeat_3_attr_website"];
          attraction.attr_repeat_3_attr_picture = [*rawData valueForKey:@"attr_repeat_3_attr_picture"];
          attraction.attr_repeat_3_attr_descript = [*rawData valueForKey:@"attr_repeat_3_attr_descript"];
    attraction.attr_repeat_4_attr_name = [*rawData valueForKey:@"attr_repeat_4_attr_name"];
    attraction.attr_repeat_4_attr_website = [*rawData valueForKey:@"attr_repeat_4_attr_website"];
    attraction.attr_repeat_4_attr_picture = [*rawData valueForKey:@"attr_repeat_4_attr_picture"];
    attraction.attr_repeat_4_attr_descript = [*rawData valueForKey:@"attr_repeat_4_attr_descript"];
    
    
        //attr_repeat_0_attr_map
        
        NSString *attr_repeat_0_attr_map_string = [*rawData valueForKey:@"attr_repeat_0_attr_map"];
         attraction.attr_repeat_0_attr_map = [CommonUtils getAddress: attr_repeat_0_attr_map_string];
        
          //attr_repeat_1_attr_map
        
        NSString *attr_repeat_1_attr_map_string = [*rawData valueForKey:@"attr_repeat_1_attr_map"];
         attraction.attr_repeat_1_attr_map = [CommonUtils getAddress: attr_repeat_1_attr_map_string];
    //attr_repeat_2_attr_map
    
    NSString *attr_repeat_2_attr_map_string = [*rawData valueForKey:@"attr_repeat_2_attr_map"];
    attraction.attr_repeat_2_attr_map = [CommonUtils getAddress: attr_repeat_2_attr_map_string];
           //attr_repeat_3_attr_map
        
        NSString *attr_repeat_3_attr_map_string = [*rawData valueForKey:@"attr_repeat_3_attr_map"];
         attraction.attr_repeat_3_attr_map = [CommonUtils getAddress: attr_repeat_3_attr_map_string];
    //attr_repeat_4_attr_map
    
    NSString *attr_repeat_4_attr_map_string = [*rawData valueForKey:@"attr_repeat_4_attr_map"];
    attraction.attr_repeat_4_attr_map = [CommonUtils getAddress: attr_repeat_4_attr_map_string];
    
          return attraction;


}




@end
