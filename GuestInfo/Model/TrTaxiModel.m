//
//  TrTaxiModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "TrTaxiModel.h"
#import "CommonUtils.h"

TrTaxiModel *g_trTaxi;

@implementation TrTaxiModel


@synthesize trt_name;
@synthesize trt_phone;
//@property (strong) NSString *trt_address_address;
//@property (strong) NSString *trt_address_lat;
//@property (strong) NSString *trt_address_lng;
@synthesize trt_website;
@synthesize trt_email;
@synthesize trt_address;

- (instancetype) init;{
    
    trt_name = @"";
    trt_phone = @"";
    trt_website = @"";
    trt_email = @"";
    trt_address = [[AddressModel alloc] init];
    
    return self;
}
+ (TrTaxiModel *) parseTrTaxi: (id *) rawData{
    
    TrTaxiModel *trTaxi = [[TrTaxiModel alloc] init];
    trTaxi.trt_name = [*rawData valueForKey:@"trt_name"];
    trTaxi.trt_phone = [*rawData valueForKey:@"trt_phone"];
    trTaxi.trt_website = [*rawData valueForKey:@"trt_website"];
    trTaxi.trt_email = [*rawData valueForKey:@"trt_email"];
  
    //trt_address
    NSString *trt_address_string = [*rawData valueForKey:@"trt_address"];
    
    trTaxi.trt_address = [CommonUtils getAddress: trt_address_string];
       return trTaxi;
}

@end
