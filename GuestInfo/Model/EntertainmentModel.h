//
//  EntertainmentModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/15/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface EntertainmentModel : NSObject

@property (strong) NSString *ent;
@property (strong) NSString *ent_0_ent_type;
@property (strong) NSString *ent_0_ent_name;
@property (strong) NSString *ent_0_ent_website;
@property (strong) NSString *ent_0_ent_phone;
@property (strong) AddressModel *ent_0_ent_map;

@property (strong) NSString *ent_1_ent_type;
@property (strong) NSString *ent_1_ent_name;
@property (strong) NSString *ent_1_ent_website;
@property (strong) NSString *ent_1_ent_phone;
@property (strong) AddressModel *ent_1_ent_map;

@property (strong) NSString *ent_2_ent_type;
@property (strong) NSString *ent_2_ent_name;
@property (strong) NSString *ent_2_ent_website;
@property (strong) NSString *ent_2_ent_phone;

@property (strong) NSString *ent_3_ent_type;
@property (strong) NSString *ent_3_ent_name;
@property (strong) NSString *ent_3_ent_website;
@property (strong) NSString *ent_3_ent_phone;
@property (strong) AddressModel *ent_3_ent_map;

@property (strong) NSString *ent_4_ent_type;
@property (strong) NSString *ent_4_ent_name;
@property (strong) NSString *ent_4_ent_website;
@property (strong) NSString *ent_4_ent_phone;
@property (strong) AddressModel *ent_4_ent_map;

@property (strong) NSString *ent_5_ent_type;
@property (strong) NSString *ent_5_ent_name;
@property (strong) NSString *ent_5_ent_website;
@property (strong) NSString *ent_5_ent_phone;
@property (strong) AddressModel *ent_5_ent_map;

@property (strong) NSString *ent_6_ent_type;
@property (strong) NSString *ent_6_ent_name;
@property (strong) NSString *ent_6_ent_website;
@property (strong) NSString *ent_6_ent_phone;
@property (strong) AddressModel *ent_6_ent_map;

@property (strong) NSString *ent_7_ent_type;
@property (strong) NSString *ent_7_ent_name;
@property (strong) NSString *ent_7_ent_website;
@property (strong) NSString *ent_7_ent_phone;
@property (strong) AddressModel *ent_7_ent_map;

@property (strong) NSString *ent_8_ent_type;
@property (strong) NSString *ent_8_ent_name;
@property (strong) NSString *ent_8_ent_website;
@property (strong) NSString *ent_8_ent_phone;
@property (strong) AddressModel *ent_8_ent_map;

- (instancetype) init;
+ (EntertainmentModel *) parseEntertainment: (id *) rawData;

@end
