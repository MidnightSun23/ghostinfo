//
//  FoodandDrinkModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "FoodandDrinkModel.h"
FoodandDrinkModel * g_foodandDrink;

@implementation FoodandDrinkModel

@synthesize fdb_time;
@synthesize fdb_where;
@synthesize fdl_time;
@synthesize fdl_where;
@synthesize fdd_time;
@synthesize fdd_where;

-(instancetype) init{
    
    fdb_time = @"";
    fdb_where = @"";
    fdl_time = @"";
    fdl_where = @"";
    fdd_time = @"";
    fdd_where = @"";

    return  self;
}

+(FoodandDrinkModel *) parseFoodandDrink:(id *) rawData;{
    
    FoodandDrinkModel *fd = [[FoodandDrinkModel alloc] init];
    fd.fdb_time = [*rawData valueForKey:@"fdb_time"];
    fd.fdb_where = [*rawData valueForKey:@"fdb_where"];
    fd.fdl_time = [*rawData valueForKey:@"fdl_time"];
    fd.fdl_where = [*rawData valueForKey:@"fdl_where"];
    fd.fdd_time = [*rawData valueForKey:@"fdd_time"];
    fd.fdd_where = [*rawData valueForKey:@"fdd_where"];
    
    return fd;
    
}

@end
