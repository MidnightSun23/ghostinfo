//
//  ComfortModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/15/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ComfortModel : NSObject

@property (strong) NSString *com_laun_details;
@property (strong) NSString *com_lug_details;
@property (strong) NSString *com_hair_details;
@property (strong) NSString *com_ct_details;
@property (strong) NSString *com_room_details;


-(instancetype) init;
+(ComfortModel *) parseComfort:(id *) rawData;


@end
