//
//  TrTaxiModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface TrTaxiModel : NSObject

@property (strong) NSString *trt_name;
@property (strong) NSString *trt_phone;

//@property (strong) NSString *trt_address_address;
//@property (strong) NSString *trt_address_lat;
//@property (strong) NSString *trt_address_lng;
@property (strong) NSString *trt_website;
@property (strong) NSString *trt_email;

@property (strong) AddressModel *trt_address;

- (instancetype) init;
+ (TrTaxiModel *) parseTrTaxi: (id *) rawData;


@end
