//
//  EntertainmentModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/15/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "EntertainmentModel.h"
#import "CommonUtils.h"

EntertainmentModel *g_entertainment;

@implementation EntertainmentModel

@synthesize ent;
@synthesize ent_0_ent_type;
@synthesize ent_0_ent_name;
@synthesize ent_0_ent_website;
@synthesize ent_0_ent_phone;
@synthesize ent_0_ent_map;

@synthesize ent_1_ent_type;
@synthesize ent_1_ent_name;
@synthesize ent_1_ent_website;
@synthesize ent_1_ent_phone;
@synthesize ent_1_ent_map;

@synthesize ent_2_ent_type;
@synthesize ent_2_ent_name;
@synthesize ent_2_ent_website;
@synthesize ent_2_ent_phone;

@synthesize ent_3_ent_type;
@synthesize ent_3_ent_name;
@synthesize ent_3_ent_website;
@synthesize ent_3_ent_phone;
@synthesize ent_3_ent_map;

@synthesize ent_4_ent_type;
@synthesize ent_4_ent_name;
@synthesize ent_4_ent_website;
@synthesize ent_4_ent_phone;
@synthesize ent_4_ent_map;

@synthesize ent_5_ent_type;
@synthesize ent_5_ent_name;
@synthesize ent_5_ent_website;
@synthesize ent_5_ent_phone;
@synthesize ent_5_ent_map;

@synthesize ent_6_ent_type;
@synthesize ent_6_ent_name;
@synthesize ent_6_ent_website;
@synthesize ent_6_ent_phone;
@synthesize ent_6_ent_map;

@synthesize ent_7_ent_type;
@synthesize ent_7_ent_name;
@synthesize ent_7_ent_website;
@synthesize ent_7_ent_phone;
@synthesize ent_7_ent_map;

@synthesize ent_8_ent_type;
@synthesize ent_8_ent_name;
@synthesize ent_8_ent_website;
@synthesize ent_8_ent_phone;
@synthesize ent_8_ent_map;

- (instancetype) init{
    
     ent = @"";
     ent_0_ent_type= @"";
     ent_0_ent_name= @"";
     ent_0_ent_website= @"";
     ent_0_ent_phone= @"";
    
     ent_1_ent_type= @"";
     ent_1_ent_name= @"";
     ent_1_ent_website= @"";
     ent_1_ent_phone= @"";
    
     ent_2_ent_type= @"";
     ent_2_ent_name= @"";
     ent_2_ent_website= @"";
     ent_2_ent_phone= @"";
    
     ent_3_ent_type= @"";
     ent_3_ent_name= @"";
     ent_3_ent_website= @"";
     ent_3_ent_phone= @"";
    
     ent_4_ent_type= @"";
     ent_4_ent_name= @"";
     ent_4_ent_website= @"";
     ent_4_ent_phone= @"";
    
     ent_5_ent_type= @"";
     ent_5_ent_name= @"";
     ent_5_ent_website= @"";
     ent_5_ent_phone= @"";
    
     ent_6_ent_type= @"";
     ent_6_ent_name= @"";
     ent_6_ent_website= @"";
     ent_6_ent_phone= @"";
    
     ent_7_ent_type= @"";
     ent_7_ent_name= @"";
     ent_7_ent_website= @"";
     ent_7_ent_phone= @"";
    
     ent_8_ent_type= @"";
     ent_8_ent_name= @"";
     ent_8_ent_website= @"";
     ent_8_ent_phone= @"";
    
    
    ent_0_ent_map= [[AddressModel alloc] init];
    ent_1_ent_map= [[AddressModel alloc] init];
    ent_3_ent_map= [[AddressModel alloc] init];
    ent_4_ent_map= [[AddressModel alloc] init];
    ent_5_ent_map= [[AddressModel alloc] init];
    ent_6_ent_map= [[AddressModel alloc] init];
    ent_7_ent_map= [[AddressModel alloc] init];
    ent_8_ent_map= [[AddressModel alloc] init];
    
    return  self;
    
}
+ (EntertainmentModel *) parseEntertainment: (id *) rawData{
    
    EntertainmentModel *entertainment = [[EntertainmentModel alloc] init];
    
    entertainment.ent= [*rawData valueForKey: @"ent"];
    entertainment.ent_0_ent_type= [*rawData valueForKey: @"ent_0_ent_type"];
    entertainment.ent_0_ent_name= [*rawData valueForKey: @"ent_0_ent_name"];
    entertainment.ent_0_ent_website= [*rawData valueForKey: @"ent_0_ent_website"];
    entertainment.ent_0_ent_phone= [*rawData valueForKey: @"ent_0_ent_phone"];
    
    entertainment.ent_1_ent_type= [*rawData valueForKey: @"ent_1_ent_type"];
   entertainment. ent_1_ent_name= [*rawData valueForKey: @"ent_1_ent_name"];
    entertainment.ent_1_ent_website= [*rawData valueForKey: @"ent_1_ent_website"];
    entertainment.ent_1_ent_phone= [*rawData valueForKey: @"ent_1_ent_phone"];
    
    entertainment.ent_2_ent_type= [*rawData valueForKey: @"ent_2_ent_type"];
    entertainment.ent_2_ent_name= [*rawData valueForKey: @"ent_2_ent_name"];
    entertainment.ent_2_ent_website= [*rawData valueForKey: @"ent_2_ent_website"];
    entertainment.ent_2_ent_phone= [*rawData valueForKey: @"ent_2_ent_phone"];
    
    entertainment.ent_3_ent_type= [*rawData valueForKey: @"ent_3_ent_type"];
    entertainment.ent_3_ent_name= [*rawData valueForKey: @"ent_3_ent_name"];
    entertainment.ent_3_ent_website= [*rawData valueForKey: @"ent_3_ent_website"];
   entertainment. ent_3_ent_phone= [*rawData valueForKey: @"ent_3_ent_phone"];
    
    entertainment.ent_4_ent_type= [*rawData valueForKey: @"ent_4_ent_type"];
    entertainment.ent_4_ent_name= [*rawData valueForKey: @"ent_4_ent_name"];
    entertainment.ent_4_ent_website= [*rawData valueForKey: @"ent_4_ent_website"];
    entertainment.ent_4_ent_phone= [*rawData valueForKey: @"ent_4_ent_phone"];
    
    entertainment.ent_5_ent_type= [*rawData valueForKey: @"ent_5_ent_type"];
    entertainment.ent_5_ent_name= [*rawData valueForKey: @"ent_5_ent_name"];
    entertainment.ent_5_ent_website= [*rawData valueForKey: @"ent_5_ent_website"];
    entertainment.ent_5_ent_phone= [*rawData valueForKey: @"ent_5_ent_phone"];
    
    entertainment.ent_6_ent_type= [*rawData valueForKey: @"ent_6_ent_type"];
    entertainment.ent_6_ent_name= [*rawData valueForKey: @"ent_6_ent_name"];
    entertainment.ent_6_ent_website= [*rawData valueForKey: @"ent_6_ent_website"];
    entertainment.ent_6_ent_phone= [*rawData valueForKey: @"ent_6_ent_phone"];
    
    entertainment.ent_7_ent_type= [*rawData valueForKey: @"ent_7_ent_type"];
    entertainment.ent_7_ent_name= [*rawData valueForKey: @"ent_7_ent_name"];
    entertainment.ent_7_ent_website= [*rawData valueForKey: @"ent_7_ent_website"];
    entertainment.ent_7_ent_phone= [*rawData valueForKey: @"ent_7_ent_phone"];
    
    entertainment.ent_8_ent_type= [*rawData valueForKey: @"ent_8_ent_type"];
    entertainment.ent_8_ent_name= [*rawData valueForKey: @"ent_8_ent_name"];
    entertainment.ent_8_ent_website= [*rawData valueForKey: @"ent_8_ent_website"];
    entertainment.ent_8_ent_phone= [*rawData valueForKey: @"ent_8_ent_phone"];
    
    
    //ent_0_ent_map
    
    NSString *ent_0_ent_map_string = [*rawData valueForKey:@"ent_0_ent_map"];
    entertainment.ent_0_ent_map = [CommonUtils getAddress: ent_0_ent_map_string];
       //ent_1_ent_map
    
    NSString *ent_1_ent_map_string = [*rawData valueForKey:@"ent_1_ent_map"];
    entertainment.ent_1_ent_map = [CommonUtils getAddress: ent_1_ent_map_string];
      //ent_3_ent_map
    
    NSString *ent_3_ent_map_string = [*rawData valueForKey:@"ent_3_ent_map"];
    entertainment.ent_3_ent_map = [CommonUtils getAddress: ent_3_ent_map_string];
    
    //ent_4_ent_map
    
    NSString *ent_4_ent_map_string = [*rawData valueForKey:@"ent_4_ent_map"];
    entertainment.ent_4_ent_map = [CommonUtils getAddress: ent_4_ent_map_string];
      //ent_5_ent_map
    
    NSString *ent_5_ent_map_string = [*rawData valueForKey:@"ent_5_ent_map"];
    entertainment.ent_5_ent_map = [CommonUtils getAddress: ent_5_ent_map_string];
       //ent_6_ent_map
    
    NSString *ent_6_ent_map_string = [*rawData valueForKey:@"ent_6_ent_map"];
    entertainment.ent_6_ent_map = [CommonUtils getAddress: ent_6_ent_map_string];
      //ent_7_ent_map
    
    NSString *ent_7_ent_map_string = [*rawData valueForKey:@"ent_7_ent_map"];
    entertainment.ent_7_ent_map = [CommonUtils getAddress: ent_7_ent_map_string];
      //ent_8_ent_map
    
    NSString *ent_8_ent_map_string = [*rawData valueForKey:@"ent_8_ent_map"];
    entertainment.ent_8_ent_map = [CommonUtils getAddress: ent_8_ent_map_string];
       return  entertainment;
}



@end
