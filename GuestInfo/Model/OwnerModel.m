//
//  OwnerModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "OwnerModel.h"
#import "CommonUtils.h"
OwnerModel *g_owner;

@implementation OwnerModel

@synthesize o_firstname;
@synthesize o_lastname;
@synthesize o_address;
@synthesize o_zip;
@synthesize o_town;
@synthesize o_country;
@synthesize o_telephone;
@synthesize o_mobile;
@synthesize o_email;
@synthesize o_skype;
@synthesize o_facebook;
@synthesize o_twitter;
@synthesize o_language;

-(instancetype) init{
    
   o_firstname= @"";
   o_lastname= @"";
   o_address= @"";
   o_zip= @"";
   o_town= @"";
   o_country= @"";
   o_telephone= @"";
   o_mobile= @"";
   o_email= @"";
   o_skype= @"";
   o_facebook= @"";
   o_twitter= @"";
   o_language = [[LanguageModel alloc] init];
    
    return self;
}
+(OwnerModel *) parseOwner:(id *) rawData{
    
    OwnerModel *owner = [[OwnerModel alloc] init];
    owner.o_firstname = [*rawData valueForKey:@"o_firstname"];
    owner.o_lastname = [*rawData valueForKey:@"o_lastname"];
    owner.o_address = [*rawData valueForKey:@"o_address"];
    owner.o_zip = [*rawData valueForKey:@"o_zip"];
    owner.o_town = [*rawData valueForKey:@"o_town"];
    owner.o_country = [*rawData valueForKey:@"o_country"];
    owner.o_telephone = [*rawData valueForKey:@"o_telephone"];
    owner.o_mobile = [*rawData valueForKey:@"o_mobile"];
    owner.o_email = [*rawData valueForKey:@"o_email"];
    owner.o_skype = [*rawData valueForKey:@"o_skype"];
    owner.o_facebook = [*rawData valueForKey:@"o_facebook"];
    owner.o_twitter = [*rawData valueForKey:@"o_twitter"];
    
    NSString *o_language_string = [*rawData valueForKey:@"o_language"];
    owner.o_language = [CommonUtils getLanguage: o_language_string];
    
    return owner;

}

@end
