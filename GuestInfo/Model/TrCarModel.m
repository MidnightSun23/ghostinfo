//
//  TrCarModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "TrCarModel.h"
#import "CommonUtils.h"
TrCarModel *g_trCar;

@implementation TrCarModel

@synthesize trc_name;
@synthesize trc_phone;
@synthesize trc_website;
@synthesize trc_email;
@synthesize trc_address;

- (instancetype) init{
    
    trc_name = @"";
    trc_phone = @"";
    trc_website = @"";
    trc_email = @"";
    trc_address = [[AddressModel alloc] init];
    
    return self;
    
}
+ (TrCarModel *) parseTrCar: (id *) rawData{
    
    TrCarModel *trCar = [[TrCarModel alloc] init];
    
    trCar.trc_name = [*rawData valueForKey:@"trc_name"];
    trCar.trc_phone = [*rawData valueForKey:@"trc_phone"];
    trCar.trc_website = [*rawData valueForKey:@"trc_website"];
    trCar.trc_email = [*rawData valueForKey:@"trc_email"];
    
    //trc_address
    
    NSString *trc_address_string = [*rawData valueForKey:@"trc_address"];
 
    
    trCar.trc_address = [CommonUtils getAddress: trc_address_string];
    
       return trCar;
}

@end
