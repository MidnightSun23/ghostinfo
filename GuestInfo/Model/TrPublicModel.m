//
//  TrPublicModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/10/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "TrPublicModel.h"
TrPublicModel *g_trPublic;

@implementation TrPublicModel

@synthesize  trp_bus;
@synthesize  trp_metro;
@synthesize  trp_train;

-(instancetype) init{
    
    trp_bus = @"";
    trp_metro = @"";
    trp_train = @"";
    
    return self;
}
+(TrPublicModel *) parseTrPublic:(id *) rawData{
    
    TrPublicModel *trPublic =[[TrPublicModel alloc] init];
    trPublic.trp_bus = [*rawData valueForKey:@"trp_bus"];
    trPublic.trp_metro = [*rawData valueForKey:@"trp_metro"];
    trPublic.trp_train = [*rawData valueForKey:@"trp_train"];
    
    return trPublic;
    
}

@end
