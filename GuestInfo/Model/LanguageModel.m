//
//  LanguageModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/25/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "LanguageModel.h"

@implementation LanguageModel

@synthesize languageArray;

-(instancetype) init{
    
    languageArray = [[NSMutableArray alloc] init];
    
    return self;
}

@end
