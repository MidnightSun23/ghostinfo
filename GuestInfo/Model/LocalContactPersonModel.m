//
//  LocalContactPersonModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "LocalContactPersonModel.h"
#import "CommonUtils.h"

LocalContactPersonModel *g_localContactPerson;

@implementation LocalContactPersonModel

@synthesize  l_firstname;
@synthesize l_lastname;
@synthesize l_address;
@synthesize l_zip;
@synthesize l_town;
@synthesize l_country;
@synthesize l_telephone;
@synthesize l_mobile;
@synthesize l_email;
@synthesize l_skype;
@synthesize l_facebook;
@synthesize l_twitter;
@synthesize l_language;

-(instancetype ) init{
    
    l_firstname= @"";
    l_lastname= @"";
    l_address= @"";
    l_zip= @"";
    l_town= @"";
    l_country= @"";
    l_telephone= @"";
    l_mobile= @"";
    l_email= @"";
    l_skype= @"";
    l_facebook= @"";
    l_twitter= @"";
    l_language = [[LanguageModel alloc] init];
    
    return self;
}


+(LocalContactPersonModel *) parseLocalContactPerson:(id *) rawData{
    
    LocalContactPersonModel *localContactPerson = [[LocalContactPersonModel alloc] init];
    localContactPerson.l_firstname = [*rawData valueForKey:@"l_firstname"];
    localContactPerson.l_lastname = [*rawData valueForKey:@"l_lastname"];
    localContactPerson.l_address = [*rawData valueForKey:@"l_address"];
    localContactPerson.l_zip = [*rawData valueForKey:@"l_zip"];
    localContactPerson.l_town = [*rawData valueForKey:@"l_town"];
    localContactPerson.l_country = [*rawData valueForKey:@"l_country"];
    localContactPerson.l_telephone = [*rawData valueForKey:@"l_telephone"];
    localContactPerson.l_mobile = [*rawData valueForKey:@"l_mobile"];
    localContactPerson.l_email = [*rawData valueForKey:@"l_email"];
    localContactPerson.l_skype = [*rawData valueForKey:@"l_skype"];
    localContactPerson.l_facebook = [*rawData valueForKey:@"l_facebook"];
    localContactPerson.l_twitter = [*rawData valueForKey:@"l_twitter"];
    
    NSString *l_language_string = [*rawData valueForKey:@"l_language"];
    localContactPerson.l_language = [CommonUtils getLanguage: l_language_string];
    
    
    return localContactPerson;
}

@end
