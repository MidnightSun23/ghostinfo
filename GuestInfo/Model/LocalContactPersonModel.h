//
//  LocalContactPersonModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LanguageModel.h"

@interface LocalContactPersonModel : NSObject

@property (strong) NSString *l_firstname;
@property (strong) NSString *l_lastname;
@property (strong) NSString *l_address;
@property (strong) NSString *l_zip;
@property (strong) NSString *l_town;
@property (strong) NSString *l_country;
@property (strong) NSString *l_telephone;
@property (strong) NSString *l_mobile;
@property (strong) NSString *l_email;
@property (strong) NSString *l_skype;
@property (strong) NSString *l_facebook;
@property (strong) NSString *l_twitter;
@property (strong) LanguageModel *l_language;

-(instancetype) init;
+(LocalContactPersonModel *) parseLocalContactPerson:(id *) rawData;


@end
