//
//  ServicesModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/17/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "ServicesModel.h"
#import "Constants.h"
#import "CommonUtils.h"

ServicesModel *g_services;

@implementation ServicesModel


@synthesize ser_checkintime;
@synthesize ser_checkouttime;
@synthesize ser_atm_address;
@synthesize ser_wifi_pass;
@synthesize ser_wifi_details;
@synthesize ser_park_details;
@synthesize ser_tv_details;


@synthesize ser_atm_location;
@synthesize ser_park_map;



- (instancetype) init{
    
    ser_checkintime= @"";
    ser_checkouttime= @"";
    ser_atm_address= @"";
    ser_wifi_pass= @"";
    ser_wifi_details= @"";
    ser_park_details= @"";
    ser_tv_details= @"";
    
    ser_atm_location= [[AddressModel alloc] init];
    ser_park_map= [[AddressModel alloc] init];
    
    return self;
}
+ (ServicesModel *) parseServices: (id *) rawData{
    
    ServicesModel *services = [[ServicesModel alloc] init];
    
    services.ser_checkintime = [*rawData valueForKey:@"ser_checkintime"];
    services.ser_checkouttime = [*rawData valueForKey:@"ser_checkouttime"];
    services.ser_atm_address = [*rawData valueForKey:@"ser_atm_address"];
    services.ser_wifi_pass = [*rawData valueForKey:@"ser_wifi_pass"];
    services.ser_wifi_details = [*rawData valueForKey:@"ser_wifi_details"];
    services.ser_park_details = [*rawData valueForKey:@"ser_park_details"];
    services.ser_tv_details = [*rawData valueForKey:@"ser_tv_details"];
    
    //ser_atm_location
    
    NSString *ser_atm_location_string = [*rawData valueForKey:@"ser_atm_location"];
    services.ser_atm_location = [CommonUtils getAddress: ser_atm_location_string];
    
    //ser_park_map
    
    NSString *ser_park_map_string = [*rawData valueForKey:@"ser_park_map"];
    services.ser_park_map = [CommonUtils getAddress: ser_park_map_string];
       return services;
}



@end
