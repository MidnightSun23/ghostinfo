//
//  TrPublicModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/10/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrPublicModel : NSObject

@property (strong) NSString *trp_bus;
@property (strong) NSString *trp_metro;
@property (strong) NSString *trp_train;

-(instancetype) init;
+(TrPublicModel *) parseTrPublic:(id *) rawData;
    



@end
