//
//  AddressModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "AddressModel.h"


@implementation AddressModel

@synthesize map_address;
@synthesize map_address_lat;
@synthesize map_address_lng;

-(instancetype) init{
    
    map_address = @"";
    map_address_lat = @"";
    map_address_lng = @"";;
    
    return self;
}

@end
