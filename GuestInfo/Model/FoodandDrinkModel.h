//
//  FoodandDrinkModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FoodandDrinkModel : NSObject

@property (strong) NSString *fdb_time;
@property (strong) NSString *fdb_where;
@property (strong) NSString *fdl_time;
@property (strong) NSString *fdl_where;
@property (strong) NSString *fdd_time;
@property (strong) NSString *fdd_where;

-(instancetype) init;
+(FoodandDrinkModel *) parseFoodandDrink:(id *) rawData;

@end
