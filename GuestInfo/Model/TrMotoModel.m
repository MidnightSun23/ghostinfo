//
//  TrMotoModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "TrMotoModel.h"
#import "CommonUtils.h"

TrMotoModel *g_trMoto;

@implementation TrMotoModel

@synthesize trm_name;
@synthesize trm_phone;
@synthesize trm_website;
@synthesize trm_email;
@synthesize trm_address;

- (instancetype) init{
    
    trm_name = @"";
    trm_phone = @"";
    trm_website = @"";
    trm_email = @"";
    trm_address = [[AddressModel alloc] init];
    
    return self;
}
+ (TrMotoModel *) parseTrMoto: (id *) rawData{
    
    TrMotoModel *trMoto = [[TrMotoModel alloc] init];
    trMoto.trm_name = [*rawData valueForKey:@"trm_name"];
    trMoto.trm_phone = [*rawData valueForKey:@"trm_phone"];
    trMoto.trm_website = [*rawData valueForKey:@"trm_website"];
    trMoto.trm_email = [*rawData valueForKey:@"trm_email"];
    
    //trm_address
    NSString *trm_address_string = [*rawData valueForKey:@"trm_address"];
    trMoto.trm_address = [CommonUtils getAddress: trm_address_string];
    
      return trMoto;
}


@end
