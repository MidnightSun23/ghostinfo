//
//  RbsModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/15/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AddressModel.h"

@interface RbsModel : NSObject

@property (strong) NSString *rsb;

@property (strong) NSString *rsb_0_rbs_type;
@property (strong) NSString *rsb_0_rbs_name;
@property (strong) NSString *rsb_0_rbs_website;
@property (strong) NSString *rsb_0_rbs_phone;
@property (strong) AddressModel *rsb_0_rbs_map;

@property (strong) NSString *rsb_1_rbs_type;
@property (strong) NSString *rsb_1_rbs_name;
@property (strong) NSString *rsb_1_rbs_website;
@property (strong) NSString *rsb_1_rbs_phone;
@property (strong) AddressModel *rsb_1_rbs_map;

@property (strong) NSString *rsb_2_rbs_type;
@property (strong) NSString *rsb_2_rbs_name;
@property (strong) NSString *rsb_2_rbs_website;
@property (strong) NSString *rsb_2_rbs_phone;
@property (strong) AddressModel *rsb_2_rbs_map;

@property (strong) NSString *rsb_3_rbs_type;
@property (strong) NSString *rsb_3_rbs_name;
@property (strong) NSString *rsb_3_rbs_website;
@property (strong) NSString *rsb_3_rbs_phone;
@property (strong) AddressModel *rsb_3_rbs_map;

@property (strong) NSString *rsb_4_rbs_type;
@property (strong) NSString *rsb_4_rbs_name;
@property (strong) NSString *rsb_4_rbs_website;
@property (strong) NSString *rsb_4_rbs_phone;
@property (strong) AddressModel *rsb_4_rbs_map;

@property (strong) NSString *rsb_5_rbs_type;
@property (strong) NSString *rsb_5_rbs_name;
@property (strong) NSString *rsb_5_rbs_website;
@property (strong) NSString *rsb_5_rbs_phone;
@property (strong) AddressModel *rsb_5_rbs_map;

@property (strong) NSString *rsb_6_rbs_type;
@property (strong) NSString *rsb_6_rbs_name;
@property (strong) NSString *rsb_6_rbs_website;
@property (strong) NSString *rsb_6_rbs_phone;
@property (strong) AddressModel *rsb_6_rbs_map;

@property (strong) NSString *rsb_7_rbs_type;
@property (strong) NSString *rsb_7_rbs_name;
@property (strong) NSString *rsb_7_rbs_website;
@property (strong) NSString *rsb_7_rbs_phone;
@property (strong) AddressModel *rsb_7_rbs_map;

@property (strong) NSString *rsb_8_rbs_type;
@property (strong) NSString *rsb_8_rbs_name;
@property (strong) NSString *rsb_8_rbs_website;
@property (strong) NSString *rsb_8_rbs_phone;
@property (strong) AddressModel *rsb_8_rbs_map;

- (instancetype) init;
+ (RbsModel *) parseRbs: (id *) rawData;

@end
