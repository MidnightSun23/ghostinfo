//
//  ComfortModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/15/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "ComfortModel.h"

ComfortModel *g_comfort;

@implementation ComfortModel

@synthesize com_laun_details;
@synthesize com_lug_details;
@synthesize com_hair_details;
@synthesize com_ct_details;
@synthesize com_room_details;


-(instancetype) init{
    
    com_laun_details= @"";
    com_lug_details= @"";
    com_hair_details= @"";
    com_ct_details= @"";
    com_room_details= @"";
    
    return self;
}
+(ComfortModel *) parseComfort:(id *) rawData{
    
    ComfortModel *comfort = [[ComfortModel alloc] init];
    comfort.com_laun_details = [*rawData valueForKey:@"com_laun_details"];
    comfort.com_lug_details = [*rawData valueForKey:@"com_lug_details"];
    comfort.com_hair_details = [*rawData valueForKey:@"com_hair_details"];
    comfort.com_ct_details = [*rawData valueForKey:@"com_ct_details"];
    comfort.com_room_details = [*rawData valueForKey:@"com_room_details"];
    
    
    return comfort;
    
}

@end
