//
//  OwnerModel.h
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LanguageModel.h"

@interface OwnerModel : NSObject

@property (strong) NSString *o_firstname;
@property (strong) NSString *o_lastname;
@property (strong) NSString *o_address;
@property (strong) NSString *o_zip;
@property (strong) NSString *o_town;
@property (strong) NSString *o_country;
@property (strong) NSString *o_telephone;
@property (strong) NSString *o_mobile;
@property (strong) NSString *o_email;
@property (strong) NSString *o_skype;
@property (strong) NSString *o_facebook;
@property (strong) NSString *o_twitter;
@property (strong) LanguageModel *o_language;

-(instancetype) init;
+(OwnerModel *) parseOwner:(id *) rawData;

@end
