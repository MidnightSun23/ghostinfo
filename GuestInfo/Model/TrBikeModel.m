//
//  TrBikeModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/9/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "TrBikeModel.h"
#import "CommonUtils.h"
TrBikeModel *g_trBike;

@implementation TrBikeModel

@synthesize trb_name;
@synthesize trb_phone;
@synthesize trb_website;
@synthesize trb_email;
@synthesize trb_address;

- (instancetype) init{
    
    trb_name = @"";
    trb_phone = @"";
    trb_website = @"";
    trb_email = @"";
    trb_address = [[AddressModel alloc] init];
    
    return self;
    
}
+ (TrBikeModel *) parseTrBike: (id *) rawData{
    
     TrBikeModel *trBike = [[TrBikeModel alloc] init];
     trBike.trb_name = [*rawData valueForKey:@"trb_name"];
     trBike.trb_phone = [*rawData valueForKey:@"trb_phone"];
     trBike.trb_website = [*rawData valueForKey:@"trb_website"];
     trBike.trb_email = [*rawData valueForKey:@"trb_email"];
    
    //trb_address
        NSString *trb_address_string = [*rawData valueForKey:@"trb_address"];

    
    trBike.trb_address = [CommonUtils getAddress: trb_address_string];
    
       return trBike;
}


@end
