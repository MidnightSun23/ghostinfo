//
//  RbsModel.m
//  GuestInfo
//
//  Created by MidnightSun on 12/15/16.
//  Copyright © 2016 midnightsun. All rights reserved.
//

#import "RbsModel.h"
#import "CommonUtils.h"

RbsModel *g_rbs;

@implementation RbsModel

@synthesize rsb;
@synthesize rsb_0_rbs_type;
@synthesize rsb_0_rbs_name;
@synthesize rsb_0_rbs_website;
@synthesize rsb_0_rbs_phone;
@synthesize rsb_0_rbs_map;
@synthesize rsb_1_rbs_type;
@synthesize rsb_1_rbs_name;
@synthesize rsb_1_rbs_website;
@synthesize rsb_1_rbs_phone;

@synthesize rsb_1_rbs_map;
@synthesize rsb_2_rbs_type;
@synthesize rsb_2_rbs_name;
@synthesize rsb_2_rbs_website;
@synthesize rsb_2_rbs_phone;
@synthesize rsb_2_rbs_map;

@synthesize rsb_3_rbs_type;
@synthesize rsb_3_rbs_name;
@synthesize rsb_3_rbs_website;
@synthesize rsb_3_rbs_phone;
@synthesize rsb_3_rbs_map;

@synthesize rsb_4_rbs_type;
@synthesize rsb_4_rbs_name;
@synthesize rsb_4_rbs_website;
@synthesize rsb_4_rbs_phone;
@synthesize rsb_4_rbs_map;

@synthesize rsb_5_rbs_type;
@synthesize rsb_5_rbs_name;
@synthesize rsb_5_rbs_website;
@synthesize rsb_5_rbs_phone;
@synthesize rsb_5_rbs_map;

@synthesize rsb_6_rbs_type;
@synthesize rsb_6_rbs_name;
@synthesize rsb_6_rbs_website;
@synthesize rsb_6_rbs_phone;
@synthesize rsb_6_rbs_map;

@synthesize rsb_7_rbs_type;
@synthesize rsb_7_rbs_name;
@synthesize rsb_7_rbs_website;
@synthesize rsb_7_rbs_phone;
@synthesize rsb_7_rbs_map;

@synthesize rsb_8_rbs_type;
@synthesize rsb_8_rbs_name;
@synthesize rsb_8_rbs_website;
@synthesize rsb_8_rbs_phone;
@synthesize rsb_8_rbs_map;

- (instancetype) init{
    
    rsb = @"";
    rsb_0_rbs_type= @"";
    rsb_0_rbs_name= @"";
    rsb_0_rbs_website= @"";
    rsb_0_rbs_phone= @"";
    rsb_1_rbs_type= @"";
    rsb_1_rbs_name= @"";
    rsb_1_rbs_website= @"";
   
    rsb_1_rbs_phone= @"";
    rsb_2_rbs_type= @"";
    rsb_2_rbs_name= @"";
    rsb_2_rbs_website= @"";
    rsb_2_rbs_phone= @"";
    
     rsb_3_rbs_type=@"";
     rsb_3_rbs_name=@"";
     rsb_3_rbs_website=@"";
     rsb_3_rbs_phone=@"";
    
    rsb_4_rbs_type=@"";
    rsb_4_rbs_name=@"";
    rsb_4_rbs_website=@"";
    rsb_4_rbs_phone=@"";
    
    rsb_5_rbs_type=@"";
    rsb_5_rbs_name=@"";
    rsb_5_rbs_website=@"";
    rsb_5_rbs_phone=@"";
    
    rsb_6_rbs_type=@"";
    rsb_6_rbs_name=@"";
    rsb_6_rbs_website=@"";
    rsb_6_rbs_phone=@"";
    
    rsb_7_rbs_type=@"";
    rsb_7_rbs_name=@"";
    rsb_7_rbs_website=@"";
    rsb_7_rbs_phone=@"";
    
    rsb_8_rbs_type=@"";
    rsb_8_rbs_name=@"";
    rsb_8_rbs_website=@"";
    rsb_8_rbs_phone=@"";
    
    rsb_0_rbs_map =[[AddressModel alloc] init];
    rsb_1_rbs_map =[[AddressModel alloc] init];
    rsb_2_rbs_map =[[AddressModel alloc] init];
    rsb_3_rbs_map =[[AddressModel alloc] init];
    rsb_4_rbs_map =[[AddressModel alloc] init];
    rsb_5_rbs_map =[[AddressModel alloc] init];
    rsb_6_rbs_map =[[AddressModel alloc] init];
    rsb_7_rbs_map =[[AddressModel alloc] init];
    rsb_8_rbs_map =[[AddressModel alloc] init];
    
    return  self;
}

+ (RbsModel *) parseRbs: (id *) rawData{
    
    RbsModel *rbs = [[RbsModel alloc] init];
    
    rbs.rsb = [*rawData valueForKey:@"rsb"];
    
    rbs.rsb_0_rbs_type = [*rawData valueForKey: @"rsb_0_rbs_type"];
    rbs.rsb_0_rbs_name = [*rawData valueForKey: @"rsb_0_rbs_name"];
    rbs.rsb_0_rbs_website = [*rawData valueForKey: @"rsb_0_rbs_website"];
    rbs.rsb_0_rbs_phone = [*rawData valueForKey: @"rsb_0_rbs_phone"];
    rbs.rsb_1_rbs_type = [*rawData valueForKey: @"rsb_1_rbs_type"];
    rbs.rsb_1_rbs_name = [*rawData valueForKey: @"rsb_1_rbs_name"];
    
    rbs.rsb_1_rbs_website = [*rawData valueForKey: @"rsb_1_rbs_website"];
    rbs.rsb_1_rbs_phone = [*rawData valueForKey: @"rsb_1_rbs_phone"];
    rbs.rsb_2_rbs_type = [*rawData valueForKey: @"rsb_2_rbs_type"];
    rbs.rsb_2_rbs_name = [*rawData valueForKey: @"rsb_2_rbs_name"];
    rbs.rsb_2_rbs_website = [*rawData valueForKey: @"rsb_2_rbs_website"];
    rbs.rsb_2_rbs_phone = [*rawData valueForKey: @"rsb_2_rbs_phone"];
    
    rbs.rsb_3_rbs_type = [*rawData valueForKey: @"rsb_3_rbs_type"];
    rbs.rsb_3_rbs_name = [*rawData valueForKey: @"rsb_3_rbs_name"];
    rbs.rsb_3_rbs_website = [*rawData valueForKey: @"rsb_3_rbs_website"];
    rbs.rsb_3_rbs_phone = [*rawData valueForKey: @"rsb_3_rbs_phone"];

    rbs.rsb_4_rbs_type = [*rawData valueForKey: @"rsb_4_rbs_type"];
    rbs.rsb_4_rbs_name = [*rawData valueForKey: @"rsb_4_rbs_name"];
    rbs.rsb_4_rbs_website = [*rawData valueForKey: @"rsb_4_rbs_website"];
    rbs.rsb_4_rbs_phone = [*rawData valueForKey: @"rsb_4_rbs_phone"];
    
    rbs.rsb_5_rbs_type = [*rawData valueForKey: @"rsb_5_rbs_type"];
    rbs.rsb_5_rbs_name = [*rawData valueForKey: @"rsb_5_rbs_name"];
    rbs.rsb_5_rbs_website = [*rawData valueForKey: @"rsb_5_rbs_website"];
    rbs.rsb_5_rbs_phone = [*rawData valueForKey: @"rsb_5_rbs_phone"];
    
    rbs.rsb_6_rbs_type = [*rawData valueForKey: @"rsb_6_rbs_type"];
    rbs.rsb_6_rbs_name = [*rawData valueForKey: @"rsb_6_rbs_name"];
    rbs.rsb_6_rbs_website = [*rawData valueForKey: @"rsb_6_rbs_website"];
    rbs.rsb_6_rbs_phone = [*rawData valueForKey: @"rsb_6_rbs_phone"];
    
    rbs.rsb_7_rbs_type = [*rawData valueForKey: @"rsb_7_rbs_type"];
    rbs.rsb_7_rbs_name = [*rawData valueForKey: @"rsb_7_rbs_name"];
    rbs.rsb_7_rbs_website = [*rawData valueForKey: @"rsb_7_rbs_website"];
    rbs.rsb_7_rbs_phone = [*rawData valueForKey: @"rsb_7_rbs_phone"];
    
    rbs.rsb_8_rbs_type = [*rawData valueForKey: @"rsb_8_rbs_type"];
    rbs.rsb_8_rbs_name = [*rawData valueForKey: @"rsb_8_rbs_name"];
    rbs.rsb_8_rbs_website = [*rawData valueForKey: @"rsb_8_rbs_website"];
    rbs.rsb_8_rbs_phone = [*rawData valueForKey: @"rsb_8_rbs_phone"];
    
    //rsb_0_rbs_map
    NSString *rsb_0_rbs_map_string = [*rawData valueForKey:@"rsb_0_rbs_map"];
    rbs.rsb_0_rbs_map = [CommonUtils getAddress: rsb_0_rbs_map_string];
       //rsb_1_rbs_map
    NSString *rsb_1_rbs_map_string = [*rawData valueForKey:@"rsb_1_rbs_map"];
    rbs.rsb_1_rbs_map = [CommonUtils getAddress: rsb_1_rbs_map_string];
       //rsb_2_rbs_map
    NSString *rsb_2_rbs_map_string = [*rawData valueForKey:@"rsb_2_rbs_map"];
    rbs.rsb_2_rbs_map = [CommonUtils getAddress: rsb_2_rbs_map_string];
       //rsb_3_rbs_map
    NSString *rsb_3_rbs_map_string = [*rawData valueForKey:@"rsb_3_rbs_map"];
    rbs.rsb_3_rbs_map = [CommonUtils getAddress: rsb_3_rbs_map_string];
       //rsb_4_rbs_map
    NSString *rsb_4_rbs_map_string = [*rawData valueForKey:@"rsb_4_rbs_map"];
    rbs.rsb_4_rbs_map = [CommonUtils getAddress: rsb_4_rbs_map_string];
        //rsb_5_rbs_map
    NSString *rsb_5_rbs_map_string = [*rawData valueForKey:@"rsb_5_rbs_map"];
    rbs.rsb_5_rbs_map = [CommonUtils getAddress: rsb_5_rbs_map_string];
        //rsb_6_rbs_map
    
    NSString *rsb_6_rbs_map_string = [*rawData valueForKey:@"rsb_6_rbs_map"];
    rbs.rsb_6_rbs_map = [CommonUtils getAddress: rsb_6_rbs_map_string];
        //rsb_7_rbs_map
    NSString *rsb_7_rbs_map_string = [*rawData valueForKey:@"rsb_7_rbs_map"];
    rbs.rsb_7_rbs_map = [CommonUtils getAddress: rsb_7_rbs_map_string];
        //rsb_8_rbs_map
    NSString *rsb_8_rbs_map_string = [*rawData valueForKey:@"rsb_8_rbs_map"];
    rbs.rsb_8_rbs_map = [CommonUtils getAddress: rsb_8_rbs_map_string];
       return  rbs;

}

@end
